// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Types.h"
#include "Blueprint/UserWidget.h"
#include "LobbyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYER_API ALobbyPlayerController : public APlayerController
{
	GENERATED_BODY()
		ALobbyPlayerController(const FObjectInitializer& ObjectInitializer);

public:
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	UFUNCTION(BlueprintCallable, Reliable, Client, WithValidation)
		void InitialSetup();

	UFUNCTION(BlueprintCallable, Reliable, Client, WithValidation)
		void LobbySetup();

	UFUNCTION(BlueprintCallable, Reliable, Client, WithValidation)
		void AddPlayerInfo(const TArray<FPlayerInfo>& connectedPlayersInfo);

	UFUNCTION(BlueprintCallable, Reliable, Client, WithValidation)
		void UpdateLobbySettings(UTexture2D* mapImage, const FText& mapName);

	/*maybe to create or else use from multiplayergi*/
	/*UFUNCTION(BlueprintCallable, Reliable, Client, WithValidation)
		void ShowLoadingScreen();*/

	/*player is kicked, so open level for mainmenu and destroy session*/
	UFUNCTION(BlueprintCallable, Reliable, Client, WithValidation)
		void Kicked();

	void CallUpdate(FPlayerInfo playerInfo, bool changedStatus);

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation)
		void ServerCallUpdate(FPlayerInfo playerInfo, bool changedStatus);

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation)
		void AssignPlayerCharacter(UClass* chara, UTexture2D* characterImage);

	UPROPERTY(Replicated, BlueprintReadWrite)
		FPlayerInfo playerSettings;

	UPROPERTY(Replicated)
		TArray<FPlayerInfo> connectedPlayers;

	UUserWidget* lobbyRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wLobby;

	FString playerSettingsSaveSlot;
};
