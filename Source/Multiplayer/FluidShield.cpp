// Fill out your copyright notice in the Description page of Project Settings.

#include "FluidShield.h"
#include "Multiplayer.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Kismet/KismetRenderingLibrary.h"
#include "Kismet/KismetMaterialLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/Canvas.h"


// Sets default values
AFluidShield::AFluidShield(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("FluidShieldMesh"));
	Mesh->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Mesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	Mesh->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	Mesh->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	RootComponent = Mesh;

	heightState = 0;
	timeAccumulator = 0.0f;
	updateRate = 60.0f;
	lastTouchingActorPosition = FVector::ZeroVector;
	interactionDistance = 1.0f;

	OnActorBeginOverlap.AddDynamic(this, &AFluidShield::OnBeginOverlap);
	OnActorEndOverlap.AddDynamic(this, &AFluidShield::OnEndOverlap);
	OnTakePointDamage.AddDynamic(this, &AFluidShield::ReceivePointDamageL);
	OnTakeAnyDamage.AddDynamic(this, &AFluidShield::ReceiveDamage);
}

// Called when the game starts or when spawned
void AFluidShield::BeginPlay()
{
	Super::BeginPlay();
	for (int i = 0; i < heightRenderTargets.Num(); i++)
	{
		UKismetRenderingLibrary::ClearRenderTarget2D(GetWorld(), heightRenderTargets[i]);
	}
	dynMaterial = Mesh->CreateDynamicMaterialInstance(0, material);
	dynMaterial->SetTextureParameterValue(TEXT("HeightfieldNormal"), heightNormalMaterial);
	//Mesh->SetMaterial(0, dynMaterial);
}

// Called every frame
void AFluidShield::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (touchingActor != nullptr)
	{
		WalkIntoShield(touchingActor->GetActorLocation());
	}
	timeAccumulator += DeltaTime;
	while (1 / FMath::Min(updateRate, 120.0f) < timeAccumulator)
	{
		timeAccumulator -= 1 / FMath::Min(updateRate, 120.0f);
		heightState = (heightState + 1) % 3;
		UMaterialInstanceDynamic* tempDynamicW = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), heightMaterial);
		tempDynamicW->SetTextureParameterValue(TEXT("PreviousHeight1"), GetLastHeight(heightState, 1));
		tempDynamicW->SetTextureParameterValue(TEXT("PreviousHeight2"), GetLastHeight(heightState, 2));
		UCanvas* canvas;
		FVector2D screenSize;
		FDrawToRenderTargetContext context;
		UTextureRenderTarget2D* rt = GetHeightRenderTarget(heightState);
		UKismetRenderingLibrary::BeginDrawCanvasToRenderTarget(GetWorld(), GetHeightRenderTarget(heightState), canvas, screenSize, context);
		canvas->K2_DrawMaterial(tempDynamicW, FVector2D::ZeroVector, screenSize, FVector2D::ZeroVector);
		UKismetRenderingLibrary::EndDrawCanvasToRenderTarget(GetWorld(), context);
		//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, GetHeightRenderTarget(heightState)->GetName());
	}
	UMaterialInstanceDynamic* tempDynamic = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), normalMaterial);
	UTextureRenderTarget2D* target = GetHeightRenderTarget(heightState);
	tempDynamic->SetTextureParameterValue(TEXT("Heightfield"), target);
	UKismetRenderingLibrary::DrawMaterialToRenderTarget(GetWorld(), heightNormalMaterial, tempDynamic);
	dynMaterial->SetTextureParameterValue(TEXT("Heightfield"), target);
}


UTextureRenderTarget2D* AFluidShield::GetHeightRenderTarget(int index)
{
	switch (index)
	{
	case 0: return heightRenderTargets[0];
		break;
	case 1: return heightRenderTargets[1];
		break;
	case 2: return heightRenderTargets[2];
		break;
	default: return nullptr;
		break;
	}
}


UTextureRenderTarget2D* AFluidShield::GetLastHeight(int currentHeightIndex, int numFramesOld)
{
	float i = (currentHeightIndex - numFramesOld + 3) % 3;
	return GetHeightRenderTarget(i);
}


void AFluidShield::OnBeginOverlap(AActor* OverlappedActor, AActor* InstigatorPawn)
{
	if (Cast<APawn>(InstigatorPawn) != nullptr)
	{
		touchingActor = InstigatorPawn;
		lastTouchingActorPosition = InstigatorPawn->GetActorLocation();
	}
}


void AFluidShield::OnEndOverlap(AActor* OverlappedActor, AActor* InstigatorPawn)
{
	if (Cast<APawn>(InstigatorPawn) != nullptr)
	{
		touchingActor = nullptr;
	}
}


void AFluidShield::ReceivePointDamageL(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const class UDamageType* DamageType, AActor* DamageCauser)
{
	UMaterialInstanceDynamic* tempDynamic = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), forceMaterial);
	FTransform t = Mesh->GetComponentTransform().Inverse();
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, UKismetMathLibrary::TransformLocation(t, HitLocation).ToString());
	FLinearColor col = UKismetMathLibrary::Conv_VectorToLinearColor(UKismetMathLibrary::Divide_VectorFloat(UKismetMathLibrary::TransformLocation(t, HitLocation), 1000.0f));
	tempDynamic->SetVectorParameterValue(TEXT("ForcePosition"), col);
	UKismetRenderingLibrary::DrawMaterialToRenderTarget(GetWorld(), GetHeightRenderTarget(heightState), tempDynamic);
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, "Received Point Damage");
}


void AFluidShield::ReceiveDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	UMaterialInstanceDynamic* tempDynamic = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), forceMaterial);
	FTransform t = Mesh->GetComponentToWorld().Inverse();
	FLinearColor col = UKismetMathLibrary::Conv_VectorToLinearColor(UKismetMathLibrary::Divide_VectorFloat(UKismetMathLibrary::TransformLocation(t, DamagedActor->GetActorLocation()), 1000.0f));
	tempDynamic->SetVectorParameterValue(TEXT("ForcePosition"), col);
	UKismetRenderingLibrary::DrawMaterialToRenderTarget(GetWorld(), GetHeightRenderTarget(heightState), tempDynamic);
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, "Received Damage");
}


void AFluidShield::WalkIntoShield(FVector HitLocation)
{
	UMaterialInstanceDynamic* tempDynamic = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), forceMaterial);
	FTransform t = Mesh->GetComponentToWorld().Inverse();
	FLinearColor col = UKismetMathLibrary::Conv_VectorToLinearColor(UKismetMathLibrary::Divide_VectorFloat(UKismetMathLibrary::TransformLocation(t, HitLocation), 1000.0f));
	tempDynamic->SetVectorParameterValue(TEXT("ForcePosition"), col);
	float f = (HitLocation - lastTouchingActorPosition).Size()*0.006f;
	tempDynamic->SetScalarParameterValue(TEXT("ForceStrength"), f);
	UKismetRenderingLibrary::DrawMaterialToRenderTarget(GetWorld(), GetHeightRenderTarget(heightState), tempDynamic);
	lastTouchingActorPosition = HitLocation;
}


void AFluidShield::HitShield(float Damage, struct FPointDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	ReceivePointDamageL(this, Damage, EventInstigator, DamageEvent.HitInfo.Location, DamageEvent.HitInfo.GetComponent(), FName::FName(), DamageEvent.ShotDirection, Cast<UDamageType>(DamageEvent.DamageTypeClass), DamageCauser);
}