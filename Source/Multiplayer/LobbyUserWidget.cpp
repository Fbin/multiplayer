// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyUserWidget.h"
#include "Multiplayer.h"
#include "ConnectedPlayerUserWidget.h"

void ULobbyUserWidget::ClearPlayerList()
{
	playersListRef->ClearChildren();
}


void ULobbyUserWidget::UpdatePlayerWindow_Implementation(FPlayerInfo newPlayerInfo)
{
	playerRef = CreateWidget<UUserWidget>(GetWorld(), wPlayer);
	UConnectedPlayerUserWidget* ref = Cast<UConnectedPlayerUserWidget>(playerRef);
	ref->playerSettings = newPlayerInfo;
	ref->connectedPlayerName = newPlayerInfo.playerName;
	ref->selectedPlayerCharacter = newPlayerInfo.playerCharacterImage;
	ref->readyStatus = newPlayerInfo.isReady;
	playersListRef->AddChild(playerRef);
}


bool ULobbyUserWidget::UpdatePlayerWindow_Validate(FPlayerInfo newPlayerInfo)
{
	return true;
}


void ULobbyUserWidget::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to every client, no special condition required
	DOREPLIFETIME(ULobbyUserWidget, mapName);
	DOREPLIFETIME(ULobbyUserWidget, mapImage);
}