// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyGameMode.h"
#include "Multiplayer.h"
#include "PCMultiplayer.h"
#include "MultiplayerGI.h"
#include "MultiplayerCharacter.h"
#include "Kismet/GameplayStatics.h"


ALobbyGameMode::ALobbyGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	/* Assign the class types used by this gamemode */
	PlayerControllerClass = ALobbyPlayerController::StaticClass();
	//PlayerStateClass = ASPlayerState::StaticClass();
	//GameStateClass = ASGameState::StaticClass();
	//SpectatorClass = ASSpectatorPawn::StaticClass();
	//TODO
	//has to be true, else servertravel wont work for launching the game
	bActorSeamlessTraveled = true;
}

void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	if (HasAuthority())
	{
		UMultiplayerGI* gIRef = Cast<UMultiplayerGI>(GetGameInstance());
		gIRef->allPlayerController.Add(NewPlayer);
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), startPointClass, startPoints);
		ALobbyPlayerController* pCRef = Cast<ALobbyPlayerController>(NewPlayer);
		//TODO
		//allways breaks here (was because of no default values, are now set)
		pCRef->InitialSetup();
		pCRef->LobbySetup();
		pCRef->UpdateLobbySettings(gMMapImage, gMMapName);
		//TODO
		//maybe move spawn to gamestart not here or make him just rotate for preview
		RespawnPlayer(NewPlayer);
		for (int i = 0; i < players.Num(); i++)
		{
			UE_LOG(LobbyLog, Warning, TEXT("playername: %s"), *players[i].playerName);
		}
	}
	else
	{

	}
}

void ALobbyGameMode::SetPlayerCharacter(UClass* selectedCharacter, int index)
{
	players[index].playerCharacter = selectedCharacter;
}


void ALobbyGameMode::SetPlayerState(bool isReady, int index)
{
	players[index].isReady = isReady;
}


void ALobbyGameMode::SetPlayerWeapon(AWeapon* selectedWeapon, int index)
{
	players[index].standardWeapon = selectedWeapon;
}


void ALobbyGameMode::SetPlayerItem(AItem* selectedItem, int index)
{
	players[index].standardItem = selectedItem;
}


void ALobbyGameMode::SwapCharacter(APlayerController* playerController, TSubclassOf<ACharacter> newCharacter, bool changedStatus)
{
	if (changedStatus)
	{

	}
	else
	{
		if (IsValid(playerController->GetPawn()))
		{
			playerController->GetPawn()->Destroy();
		}

		int rndNr = FMath::RandRange(0, startPoints.Num() - 1);
		FVector StartLocation;
		FRotator StartRotation;
		if (startPoints.Num() != 0)
		{
			StartLocation = startPoints[rndNr]->GetActorLocation();
			StartRotation = startPoints[rndNr]->GetActorRotation();
		}
		else
		{
			StartLocation = FVector(0, 0, 0);
			StartRotation = FRotator(0, 0, 0);
		}
		FActorSpawnParameters p;
		APawn* ResultPawn = GetWorld()->SpawnActor<APawn>(newCharacter, StartLocation, StartRotation, p);
		playerController->Possess(ResultPawn);
	}
}


void ALobbyGameMode::ServerSwapCharacter_Implementation(APlayerController* playerController, TSubclassOf<ACharacter> newCharacter, bool changedStatus)
{
	SwapCharacter(playerController, newCharacter, changedStatus);
}

bool ALobbyGameMode::ServerSwapCharacter_Validate(APlayerController* playerController, TSubclassOf<ACharacter> newCharacter, bool changedStatus)
{
	return true;
}


void ALobbyGameMode::UpdateAll()
{
	UMultiplayerGI* gIRef = Cast<UMultiplayerGI>(GetGameInstance());
	currentPlayerAmount = gIRef->allPlayerController.Num();
	if (currentPlayerAmount > 0)
	{
		players.Empty();
		for (int i = 0; i < currentPlayerAmount; i++)
		{
			players.Add(Cast<ALobbyPlayerController>(gIRef->allPlayerController[i])->playerSettings);
		}
		for (int i = 0; i < currentPlayerAmount; i++)
		{
			Cast<ALobbyPlayerController>(gIRef->allPlayerController[i])->AddPlayerInfo(players);
			AddToKickList();
		}
		for (int i = 0; i < players.Num(); i++)
		{
			if (players[i].playerCharacter == DefaultPawnClass)
			{
				CanStart = false;
				break;
			}
			else
			{
				CanStart = true;
			}
		}
	}
}


void ALobbyGameMode::ServerUpdateAll_Implementation()
{
	UpdateAll();
}

bool ALobbyGameMode::ServerUpdateAll_Validate()
{
	return true;
}


void ALobbyGameMode::ServerUpdateGameSettings_Implementation(UTexture2D* mapImage, const FText& mapName, int mapId)
{
	gMMapImage = mapImage;
	gMMapName = mapName;
	gMMapId = mapId;
	UMultiplayerGI* gI = Cast<UMultiplayerGI>(GetGameInstance());
	for (int i = 0; i < gI->allPlayerController.Num(); i++)
	{
		Cast<ALobbyPlayerController>(gI->allPlayerController[i])->UpdateLobbySettings(gMMapImage, gMMapName);
	}
}

bool ALobbyGameMode::ServerUpdateGameSettings_Validate(UTexture2D* mapImage, const FText& mapName, int mapId)
{
	return true;
}


void ALobbyGameMode::RespawnPlayer_Implementation(APlayerController* pcRef)
{
	if (IsValid(pcRef->GetPawn()))
	{
		pcRef->GetPawn()->Destroy();
	}
	int rndNr = FMath::RandRange(0, startPoints.Num() - 1);
	FVector StartLocation;
	FRotator StartRotation;
	if (startPoints.Num() != 0)
	{
		StartLocation = startPoints[rndNr]->GetActorLocation();
		StartRotation = startPoints[rndNr]->GetActorRotation();
	}
	else
	{
		StartLocation = FVector(0, 0, 0);
		StartRotation = FRotator(0, 0, 0);
	}
	FActorSpawnParameters p;
	APawn* ResultPawn = GetWorld()->SpawnActor<APawn>(DefaultPawnClass, StartLocation, StartRotation, p);
	pcRef->Possess(ResultPawn);
	
	//TODO do i have to check for authority?
	if (HasAuthority())
	{
		UpdateAll();
	}
	else
	{
		ServerUpdateAll();
	}
}

bool ALobbyGameMode::RespawnPlayer_Validate(APlayerController* pcRef)
{
	return true;
}


void ALobbyGameMode::AddToKickList()
{

}


void ALobbyGameMode::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to every client, no special condition required
	DOREPLIFETIME(ALobbyGameMode, players);
	DOREPLIFETIME(ALobbyGameMode, currentPlayerAmount);
	DOREPLIFETIME(ALobbyGameMode, gMMapImage);
	DOREPLIFETIME(ALobbyGameMode, gMMapName);
	DOREPLIFETIME(ALobbyGameMode, gMMapId);
}