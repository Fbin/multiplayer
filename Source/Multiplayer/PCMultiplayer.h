// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PCMultiplayer.generated.h"

/**
*
*/
UCLASS()
class MULTIPLAYER_API APCMultiplayer : public APlayerController
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wMainMenu;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wPlayerSettings;

private:
	UUserWidget* MyMainMenu;

	UUserWidget* MyPlayerSettings;

	FString PlayerSettingsSave;

	virtual void BeginPlay() override;


};
