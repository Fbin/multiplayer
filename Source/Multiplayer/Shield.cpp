// Fill out your copyright notice in the Description page of Project Settings.

#include "Shield.h"
#include "Components/StaticMeshComponent.h"
#include "Multiplayer.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Components/TimelineComponent.h"
#include "Kismet/KismetRenderingLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AShield::AShield(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("ShieldMesh"));
	Mesh->bReceivesDecals = true;
	Mesh->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Mesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	Mesh->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	RootComponent = Mesh;
}

// Called when the game starts or when spawned
void AShield::BeginPlay()
{
	Super::BeginPlay();
	FOnTimelineFloat ProgressFunction;

	ProgressFunction.BindUFunction(this, FName("HandleProgress"));
	MyTimeline.AddInterpFloat(CurveFloat, ProgressFunction);
}

// Called every frame
void AShield::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MyTimeline.TickTimeline(DeltaTime);
}


UStaticMeshComponent* AShield::GetShieldMesh() const
{
	return Mesh;
}


void AShield::HitShield(float Damage, struct FPointDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	dynMaterial = Mesh->CreateDynamicMaterialInstance(0);
	dynMaterial->SetVectorParameterValue(TEXT("position"), DamageEvent.HitInfo.ImpactPoint);
	dynMaterials.Add(dynMaterial);
	maxImpactRadius = Damage;
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), hitParticles, DamageEvent.HitInfo.ImpactPoint, UKismetMathLibrary::MakeRotFromZ(DamageEvent.HitInfo.ImpactNormal), true);
	MyTimeline.PlayFromStart();
}


void AShield::HandleProgress(float Value)
{
	impactRadius = Value * maxImpactRadius;
	dynMaterial->SetScalarParameterValue(TEXT("radius"), impactRadius);
}