// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickupActor.h"
#include "Weapon.h"
#include "WeaponPickup.generated.h"

/**
 * 
 */
UCLASS(ABSTRACT)
class MULTIPLAYER_API AWeaponPickup : public APickupActor
{
	GENERATED_BODY()
	
		AWeaponPickup(const FObjectInitializer& ObjectInitializer);

	/* Class to add to inventory when picked up */
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AWeapon> WeaponClass;

	UFUNCTION()
	virtual void OnPickup(AActor* OverlappedActor, AActor* InstigatorPawn);
};
