// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponPickup.h"
#include "Multiplayer.h"
#include "MultiplayerCharacter.h"


AWeaponPickup::AWeaponPickup(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	OnActorBeginOverlap.AddDynamic(this, &AWeaponPickup::OnPickup);
	//TODO check if actually can be removed
	/* ObjectInitializer from constructor is no longer required to create components. */
	// 	SkelMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkelMesh"));
	// 	SkelMeshComp->SetSimulatePhysics(true);
	// 	SkelMeshComp->AttachParent = GetRootComponent();
}


//TODO check if instigator or overlapped the player will be
void AWeaponPickup::OnPickup(AActor* OverlappedActor, AActor* InstigatorPawn)
{
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, "Pickup");
	AMultiplayerCharacter* MyPawn = Cast<AMultiplayerCharacter>(InstigatorPawn);
	if (MyPawn)
	{
		/* Fetch the default variables of the class we are about to pick up and check if the storage slot is available on the pawn. */
		if (MyPawn->WeaponSlotAvailable(WeaponClass->GetDefaultObject<AWeapon>()->GetStorageSlot()))
		{
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.bNoFail = true;
			AWeapon* NewWeapon = GetWorld()->SpawnActor<AWeapon>(WeaponClass, SpawnInfo);

			MyPawn->AddWeapon(NewWeapon);

			Destroy();
		}
		{
			// TODO: Add warning to user HUD.
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, "Weapon slot taken!");
		}
	}
}