// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "WeaponActor.generated.h"

/**
 * 
 */
UCLASS(ABSTRACT)
class MULTIPLAYER_API AWeaponActor : public AWeapon
{
	GENERATED_BODY()
	
private:

	/************************************************************************/
	/* Visual Handlers                                                      */
	/************************************************************************/

	void SimulateInstantHit(const FVector& Origin);

	void SpawnImpactEffects(const FHitResult& Impact);

	void SpawnTrailEffects(const FVector& EndPoint);

	/* Particle FX played when a surface is hit. */
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class AImpactEffect> ImpactTemplate;

	UPROPERTY(EditDefaultsOnly)
		FName TrailTargetParam;

	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* TrailFX;

	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* TracerFX;

	/* Minimum firing distance before spawning tracers or trails. */
	UPROPERTY(EditDefaultsOnly)
		float MinimumProjectileSpawnDistance;

	UPROPERTY(EditDefaultsOnly)
		int32 TracerRoundInterval;

	int32 BulletsShotCount;

protected:
	AWeaponActor(const class FObjectInitializer& PCIP);

	/************************************************************************/
	/* Damage Processing                                                    */
	/************************************************************************/

	virtual void FireWeapon() override;

	void DealDamage(const FHitResult& Impact, const FVector& ShootDir);

	bool ShouldDealDamage(AActor* TestActor) const;

	void ProcessInstantHit(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir);

	void ProcessInstantHitConfirmed(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir);

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerNotifyHit(const FHitResult Impact, FVector_NetQuantizeNormal ShootDir);

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerNotifyMiss(FVector_NetQuantizeNormal ShootDir);

	UPROPERTY(Transient, ReplicatedUsing = OnRep_HitLocation)
		FVector HitOriginNotify;

	UFUNCTION()
		void OnRep_HitLocation();

	/************************************************************************/
	/* Weapon Configuration                                                 */
	/************************************************************************/

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class UDamageType> DamageType;

	/* Hit verification: threshold for dot product between view direction and hit direction */
	UPROPERTY(EditDefaultsOnly)
		float AllowedViewDotHitDir;

	/* Hit verification: scale for bounding box of hit actor */
	UPROPERTY(EditDefaultsOnly)
		float ClientSideHitLeeway;

	float TotalShotsAmount;

	float TotalShotsScattering;

public:
	UPROPERTY(EditDefaultsOnly)
		float BaseHitDamage;

	float HitDamage;

	UPROPERTY(EditDefaultsOnly)
		float WeaponRange;

	/*FieldofView value for cameracomp of character*/
	UPROPERTY(EditDefaultsOnly)
		float WeaponZoom;

	/*amount of additional shot to weapon shots*/
	UPROPERTY(EditDefaultsOnly)
		float ShotsAmount;

	UPROPERTY(EditDefaultsOnly)
		float ShotScattering;

	void SetValues(float ShotAmountChange, float ShotScatteringChange);
};