// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Multiplayer.h"
#include "Types.h"
#include "ConnectedPlayerUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYER_API UConnectedPlayerUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
	
public:
	UPROPERTY(BlueprintReadWrite, Replicated)
		FPlayerInfo playerSettings;

	UPROPERTY(BlueprintReadWrite, Replicated)
		FString connectedPlayerName;

	UPROPERTY(BlueprintReadWrite, Replicated)
		UTexture2D* selectedPlayerCharacter;

	UPROPERTY(BlueprintReadWrite, Replicated)
		bool readyStatus;
};
