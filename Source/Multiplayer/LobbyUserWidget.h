// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"
#include "Multiplayer.h"
#include "Blueprint/UserWidget.h"
#include "Components/VerticalBox.h"
#include "LobbyUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYER_API ULobbyUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
		void ClearPlayerList();

	UFUNCTION(BlueprintCallable, Reliable, Client, WithValidation)
		void UpdatePlayerWindow(FPlayerInfo newPlayerInfo);

	UPROPERTY(BlueprintReadWrite, Replicated)
		FText mapName;

	UPROPERTY(BlueprintReadWrite, Replicated)
		UTexture2D* mapImage;

	UUserWidget* playerRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		TSubclassOf<class UUserWidget> wPlayer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Prefabs")
		UVerticalBox* playersListRef;
};
