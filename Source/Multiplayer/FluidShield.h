// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/TextureRenderTarget2D.h"
#include "FluidShield.generated.h"

UCLASS()
class MULTIPLAYER_API AFluidShield : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFluidShield(const FObjectInitializer& ObjectInitializer);

	UFUNCTION()
		virtual void OnBeginOverlap(AActor* OverlappedActor, AActor* InstigatorPawn);

	UFUNCTION()
		virtual void OnEndOverlap(AActor* OverlappedActor, AActor* InstigatorPawn);

	UFUNCTION()
		virtual void ReceivePointDamageL(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const class UDamageType* DamageType, AActor* DamageCauser);
	
	UFUNCTION()
		virtual void ReceiveDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void HitShield(float Damage, struct FPointDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser);

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		UStaticMeshComponent* Mesh;
	
	UPROPERTY(EditDefaultsOnly)
		UMaterialInterface* material;

	UPROPERTY(EditDefaultsOnly)
		float updateRate;

	UPROPERTY(EditDefaultsOnly)
		TArray<UTextureRenderTarget2D*> heightRenderTargets;

	UPROPERTY(EditDefaultsOnly)
		UMaterialInterface* forceMaterial;

	UPROPERTY(EditDefaultsOnly)
		UMaterialInterface* heightMaterial;

	UPROPERTY(EditDefaultsOnly)
		UTextureRenderTarget2D* heightNormalMaterial;

	UPROPERTY(EditDefaultsOnly)
		UMaterialInterface* normalMaterial;

	UMaterialInstanceDynamic* dynMaterial;
	
	int heightState;

	float timeAccumulator;

	/*TODO check if wanted fx for enter or leave shield*/
	AActor* touchingActor;

	FVector lastTouchingActorPosition;

	float interactionDistance;

private:
	UTextureRenderTarget2D* GetHeightRenderTarget(int index);

	UTextureRenderTarget2D* GetLastHeight(int currentHeightIndex, int numFramesOld);

	void WalkIntoShield(FVector HitLocation);
};
