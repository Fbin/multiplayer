// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickupActor.h"
#include "Item.h"
#include "ItemPickup.generated.h"

/**
 * 
 */
UCLASS(ABSTRACT)
class MULTIPLAYER_API AItemPickup : public APickupActor
{
	GENERATED_BODY()

	AItemPickup(const class FObjectInitializer& ObjectInitializer);
	
	/* Class to add to inventory when picked up */
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AItem> ItemClass;

	virtual void OnUsed(APawn* InstigatorPawn) override;

	UFUNCTION()
		virtual void OnPickup(AActor* OverlappedActor, AActor* InstigatorPawn);
};
