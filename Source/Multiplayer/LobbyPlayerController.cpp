// Fill out your copyright notice in the Description page of Project Settings.

#include "LobbyPlayerController.h"
#include "Multiplayer.h"
#include "MultiplayerGI.h"
#include "Kismet/GameplayStatics.h"
#include "LobbyGameMode.h"
#include "LobbyUserWidget.h"

DEFINE_LOG_CATEGORY(LobbyLog);

ALobbyPlayerController::ALobbyPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	playerSettingsSaveSlot = "PlayerSettingsSave";
}


void ALobbyPlayerController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	Cast<UMultiplayerGI>(GetGameInstance())->DestroySessionAndLeaveGame();
}

void ALobbyPlayerController::InitialSetup_Implementation()
{
	//TODO savegamecheck
	UMultiplayerGI* giRef = Cast<UMultiplayerGI>(GetGameInstance());
	if (giRef->SaveGameCheck(playerSettingsSaveSlot))
	{
		FPlayerInfo loadedInfo = giRef->LoadGame(playerSettingsSaveSlot);
		playerSettings.playerIcon = loadedInfo.playerIcon;
		playerSettings.playerName = loadedInfo.playerName;
		UE_LOG(LobbyLog, Warning, TEXT("playername: %s"), *playerSettings.playerName);
		giRef->SaveGame(playerSettingsSaveSlot, playerSettings);
	}
	else
	{
		giRef->SaveGame(playerSettingsSaveSlot, playerSettings);
	}

	if (Role == ROLE_Authority)
	{
		CallUpdate(playerSettings, false);
	}
	else
	{
		ServerCallUpdate(playerSettings, false);
	}
}


bool ALobbyPlayerController::InitialSetup_Validate()
{
	return true;
}


void ALobbyPlayerController::LobbySetup_Implementation()
{
	//TODO
	//should be same as showlobby at gameinstance -> still needed?, is called at post login of lobbygamemode
	//showmousecursor
	//create lobby widget and set lobbyRef
	if (!IsValid(lobbyRef))
	{
		lobbyRef = CreateWidget<UUserWidget>(GetWorld(), wLobby);
	}
	//add widget to viewport
	lobbyRef->AddToViewport();
	//EnableMouse();
}


bool ALobbyPlayerController::LobbySetup_Validate()
{
	return true;
}


void ALobbyPlayerController::AddPlayerInfo_Implementation(const TArray<FPlayerInfo>& connectedPlayersInfo)
{
	connectedPlayers = connectedPlayersInfo;
	if (IsValid(lobbyRef))
	{
		Cast<ULobbyUserWidget>(lobbyRef)->ClearPlayerList();
		for (int i = 0; i < connectedPlayers.Num(); i++)
		{
			Cast<ULobbyUserWidget>(lobbyRef)->UpdatePlayerWindow(connectedPlayers[i]);
		}
	}
}


bool ALobbyPlayerController::AddPlayerInfo_Validate(const TArray<FPlayerInfo>& connectedPlayersInfo)
{
	return true;
}


void ALobbyPlayerController::UpdateLobbySettings_Implementation(UTexture2D* mapImage, const FText& mapName)
{
	Cast<ULobbyUserWidget>(lobbyRef)->mapName = mapName;
	Cast<ULobbyUserWidget>(lobbyRef)->mapImage = mapImage;
}


bool ALobbyPlayerController::UpdateLobbySettings_Validate(UTexture2D* mapImage, const FText& mapName)
{
	return true;
}


void ALobbyPlayerController::Kicked_Implementation()
{
	Cast<UMultiplayerGI>(GetGameInstance())->DestroySessionAndLeaveGame();
}


bool ALobbyPlayerController::Kicked_Validate()
{
	return true;
}


void ALobbyPlayerController::CallUpdate(FPlayerInfo playerInfo, bool changedStatus)
{
	playerSettings = playerInfo;
	ALobbyGameMode* gm = Cast<ALobbyGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	gm->SwapCharacter(this, playerSettings.playerCharacter, changedStatus);
	gm->UpdateAll();
}


void ALobbyPlayerController::ServerCallUpdate_Implementation(FPlayerInfo playerInfo, bool changedStatus)
{
	CallUpdate(playerInfo, changedStatus);
}


bool ALobbyPlayerController::ServerCallUpdate_Validate(FPlayerInfo playerInfo, bool changedStatus)
{
	return true;
}


void ALobbyPlayerController::AssignPlayerCharacter_Implementation(UClass* chara, UTexture2D* characterImage)
{
	playerSettings.playerCharacter = chara;
	playerSettings.playerCharacterImage = characterImage;
	Cast<UMultiplayerGI>(GetGameInstance())->SaveGame(playerSettingsSaveSlot, playerSettings);
	ServerCallUpdate(playerSettings, false);
}


bool ALobbyPlayerController::AssignPlayerCharacter_Validate(UClass* chara, UTexture2D* characterImage)
{
	return true;
}


void ALobbyPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to every client, no special condition required
	DOREPLIFETIME(ALobbyPlayerController, playerSettings);
	DOREPLIFETIME(ALobbyPlayerController, connectedPlayers);
}