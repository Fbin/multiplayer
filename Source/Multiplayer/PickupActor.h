// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UsableActor.h"
#include "Sound/SoundCue.h"
#include "PickupActor.generated.h"

/**
 * 
 */
UCLASS(ABSTRACT)
class MULTIPLAYER_API APickupActor : public AUsableActor
{
	GENERATED_BODY()
	
	void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
		USoundCue* PickupSound;

public:
	APickupActor(const class FObjectInitializer& ObjectInitializer);

	virtual void OnUsed(APawn* InstigatorPawn) override;

	//new
	//UFUNCTION()
		//void OnRep_IsActive();

protected:
	//TODO maybe use for throw check
	//UPROPERTY(Transient, ReplicatedUsing = OnRep_IsActive)
		//bool bIsActive;

	virtual void RespawnPickup();

	//virtual void OnPickedUp();

	virtual void OnRespawned();

public:
	/* Immediately spawn on begin play */
	UPROPERTY(EditDefaultsOnly, Category = "Pickup")
		bool bStartActive;

	/* Will this item ever respawn */
	UPROPERTY(EditDefaultsOnly, Category = "Pickup")
		bool bAllowRespawn;

	UPROPERTY(EditDefaultsOnly, Category = "Pickup")
		float RespawnDelay;

	/* Extra delay randomly applied on the respawn timer */
	UPROPERTY(EditDefaultsOnly, Category = "Pickup")
		float RespawnDelayRange;

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerUsed(APawn* InstigatorPawn);

	void ServerUsed_Implementation(APawn* InstigatorPawn);

	bool ServerUsed_Validate(APawn* InstigatorPawn);

	UFUNCTION(Reliable, NetMulticast)
		void OnUsedMulticast(AActor* InstigatorPawn);

	void OnUsedMulticast_Implementation(AActor* InstigatorPawn);

	void Drop();

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerDrop();

	void ServerDrop_Implementation();

	bool ServerDrop_Validate();

	UFUNCTION(Reliable, NetMulticast)
		void OnDropMulticast();

	void OnDropMulticast_Implementation();

	void Throw();

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerThrow();

	void ServerThrow_Implementation();

	bool ServerThrow_Validate();
};
