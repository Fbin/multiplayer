// Fill out your copyright notice in the Description page of Project Settings.

#include "MultiplayerGI.h"
#include "Multiplayer.h"
#include "UserWidget.h"
#include "LobbyGameMode.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "OnlineEngineInterface.h"

UMultiplayerGI::UMultiplayerGI(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	/** Bind function for CREATING a Session */
	OnCreateSessionCompleteDelegate = FOnCreateSessionCompleteDelegate::CreateUObject(this, &UMultiplayerGI::OnCreateSessionComplete);
	OnStartSessionCompleteDelegate = FOnStartSessionCompleteDelegate::CreateUObject(this, &UMultiplayerGI::OnStartOnlineGameComplete);

	/** Bind function for FINDING a Session */
	OnFindSessionsCompleteDelegate = FOnFindSessionsCompleteDelegate::CreateUObject(this, &UMultiplayerGI::OnFindSessionsComplete);

	/** Bind function for JOINING a Session */
	OnJoinSessionCompleteDelegate = FOnJoinSessionCompleteDelegate::CreateUObject(this, &UMultiplayerGI::OnJoinSessionComplete);

	/** Bind function for DESTROYING a Session */
	OnDestroySessionCompleteDelegate = FOnDestroySessionCompleteDelegate::CreateUObject(this, &UMultiplayerGI::OnDestroySessionComplete);

	//Bind function for OnSuccess and OnFailure
	//OnSuccessDelegate = FOnFindSessionsResultDelegate::CreateUObject(this, &UMultiplayerGI::OnSuccessSessionFound);
	//OnFailureDelegate = FOnFindSessionsResultDelegate::CreateUObject(this, &UMultiplayerGI::OnFailureSessionFound);

	UE_LOG(LobbyLog, Error, TEXT("blah: %i"), 1234);
	//UE_LOG(LogWorld, Error, TEXT("blah: %i"), 1234);
}

bool UMultiplayerGI::HostSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers, FName MapName)
{
	// Get the Online Subsystem to work with
	IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		// Get the Session Interface, so we can call the "CreateSession" function on it
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid() && UserId.IsValid())
		{
			/*
			Fill in all the Session Settings that we want to use.

			There are more with SessionSettings.Set(...);
			For example the Map or the GameMode/Type.
			*/
			SessionSettings = MakeShareable(new FOnlineSessionSettings());

			SessionSettings->bIsLANMatch = bIsLAN;
			SessionSettings->bUsesPresence = bIsPresence;
			SessionSettings->NumPublicConnections = MaxNumPlayers;
			SessionSettings->NumPrivateConnections = 0;
			SessionSettings->bAllowInvites = true;
			SessionSettings->bAllowJoinInProgress = true;
			SessionSettings->bShouldAdvertise = true;
			SessionSettings->bAllowJoinViaPresence = true;
			SessionSettings->bAllowJoinViaPresenceFriendsOnly = false;

			SessionSettings->Set(SETTING_MAPNAME, /*FString("NewMap")*/MapName.ToString(), EOnlineDataAdvertisementType::ViaOnlineService);
			SessionSettings->Set(FName(TEXT("GameName")), SessionName.ToString(), EOnlineDataAdvertisementType::ViaOnlineService);

			// Set the delegate to the Handle of the SessionInterface
			OnCreateSessionCompleteDelegateHandle = Sessions->AddOnCreateSessionCompleteDelegate_Handle(OnCreateSessionCompleteDelegate);

			// Our delegate should get called when this is complete (doesn't need to be successful!)
			return Sessions->CreateSession(*UserId, SessionName, *SessionSettings);
		}
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("No OnlineSubsytem found!"));
	}

	return false;
}

void UMultiplayerGI::OnCreateSessionComplete(FName SessionName, bool bWasSuccessful)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OnCreateSessionComplete %s, %d"), *SessionName.ToString(), bWasSuccessful));

	// Get the OnlineSubsystem so we can get the Session Interface
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get the Session Interface to call the StartSession function
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			Sessions->ClearOnCreateSessionCompleteDelegate_Handle(OnCreateSessionCompleteDelegateHandle);
			if (bWasSuccessful)
			{
				// Set the StartSession delegate handle
				OnStartSessionCompleteDelegateHandle = Sessions->AddOnStartSessionCompleteDelegate_Handle(OnStartSessionCompleteDelegate);

				// Our StartSessionComplete delegate should get called after this
				Sessions->StartSession(SessionName);
			}
		}

	}
}

void UMultiplayerGI::OnStartOnlineGameComplete(FName SessionName, bool bWasSuccessful)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OnStartSessionComplete %s, %d"), *SessionName.ToString(), bWasSuccessful));

	// Get the Online Subsystem so we can get the Session Interface
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get the Session Interface to clear the Delegate
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid())
		{
			Sessions->ClearOnStartSessionCompleteDelegate_Handle(OnStartSessionCompleteDelegateHandle);
		}
	}

	//TODO check if mapName works
	if (bWasSuccessful)
	{
		/*AGameModeBase* gm = UGameplayStatics::GetGameMode(GetWorld());
		ALobbyGameMode* agm = Cast<ALobbyGameMode>(gm);
		for (int i = 0; i < agm->players.Num(); i++)
		{
			GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, agm->players[i].playerName);
		}*/
		//TODO
		FString mapString;
		SessionSettings->Get<FString>(SETTING_MAPNAME, mapString);
		FName mapName = FName(*mapString);
		//GetWorld()->ServerTravel(mapString);
		//ShowLobby();
		UGameplayStatics::OpenLevel(GetWorld(), "Lobby", true, "listen");
		//UGameplayStatics::OpenLevel(GetWorld(), mapName, true, "listen");
	}
}

void UMultiplayerGI::FindSessions(TSharedPtr<const FUniqueNetId> UserId, bool bIsLAN, bool bIsPresence)
{
	// Get the OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		// Get the SessionInterface from our OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid() && UserId.IsValid())
		{
			/*
			Fill in all the SearchSettings, like if we are searching for a LAN game and how many results we want to have!
			*/
			SessionSearch = MakeShareable(new FOnlineSessionSearch());

			SessionSearch->bIsLanQuery = bIsLAN;
			SessionSearch->MaxSearchResults = 20;
			SessionSearch->PingBucketSize = 50;

			// We only want to set this Query Setting if "bIsPresence" is true
			if (bIsPresence)
			{
				SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, bIsPresence, EOnlineComparisonOp::Equals);
			}

			TSharedRef<FOnlineSessionSearch> SearchSettingsRef = SessionSearch.ToSharedRef();

			// Set the Delegate to the Delegate Handle of the FindSession function
			OnFindSessionsCompleteDelegateHandle = Sessions->AddOnFindSessionsCompleteDelegate_Handle(OnFindSessionsCompleteDelegate);

			// Finally call the SessionInterface function. The Delegate gets called once this is finished
			Sessions->FindSessions(*UserId, SearchSettingsRef);
		}
	}
	else
	{
		OnFindSessionsComplete(false);
	}
}

void UMultiplayerGI::OnFindSessionsComplete(bool bWasSuccessful)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OFindSessionsComplete bSuccess: %d"), bWasSuccessful));
	// Get OnlineSubsystem we want to work with
	IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get SessionInterface of the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid())
		{
			Sessions->ClearOnFindSessionsCompleteDelegate_Handle(OnFindSessionsCompleteDelegateHandle);

			// Just debugging the Number of Search results. Can be displayed in UMG or something later on
			GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Num Search Results: %d"), SessionSearch->SearchResults.Num()));

			// If we have found at least 1 session, we just going to debug them. You could add them to a list of UMG Widgets, like it is done in the BP version!
			if (SessionSearch->SearchResults.Num() > 0)
			{
				// This can be customized later on with your own classes to add more information that can be set and displayed
				for (int32 SearchIdx = 0; SearchIdx < SessionSearch->SearchResults.Num(); SearchIdx++)
				{
					// OwningUserName is just the SessionName for now. I guess you can create your own Host Settings class and GameSession Class and add a proper GameServer Name here.
					// This is something you can't do in Blueprint for example!
					GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Session Number: %d | Sessionname: %s "), SearchIdx + 1, *(SessionSearch->SearchResults[SearchIdx].Session.OwningUserName)));
					GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Session Number: %d | Sessionid: %s "), SearchIdx + 1, *(SessionSearch->SearchResults[SearchIdx].Session.OwningUserId->ToString())));
					FString gameName;
					SessionSearch->SearchResults[SearchIdx].Session.SessionSettings.Get<FString>(FName("GameName"), gameName);
					GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, gameName);
					//temp_results.Add(FBlueprintSessionResult(SessionSearch->SearchResults[SearchIdx]));
				}
			}
		}
		if (bWasSuccessful && SessionSearch.IsValid())
		{
			for (auto& Result : SessionSearch->SearchResults)
			{
				FBlueprintSessionResult BPResult;
				BPResult.OnlineResult = Result;
				temp_results.Add(BPResult);
				FString gameName;
				Result.Session.SessionSettings.Get<FString>(FName("GameName"), gameName);
				GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Result added %s "), *gameName));
			}
			currentState = EFindState::OnSuccess;

			OnSuccessDelegate.Broadcast(temp_results);
		}
		else
		{
			currentState = EFindState::OnFailure;
			OnFailureDelegate.Broadcast(temp_results);
		}
	}
}

void UMultiplayerGI::OnSuccessSessionFound(TArray<FBlueprintSessionResult>& Results)
{

}
void UMultiplayerGI::OnFailureSessionFound(TArray<FBlueprintSessionResult>& Results)
{

}

bool UMultiplayerGI::JoinSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult)
{
	bool bSuccessful = false;

	// Get OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		// Get SessionInterface from the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid() && UserId.IsValid())
		{
			// Set the Handle again
			OnJoinSessionCompleteDelegateHandle = Sessions->AddOnJoinSessionCompleteDelegate_Handle(OnJoinSessionCompleteDelegate);

			// Call the "JoinSession" Function with the passed "SearchResult". The "SessionSearch->SearchResults" can be used to get such a
			// "FOnlineSessionSearchResult" and pass it. Pretty straight forward!
			bSuccessful = Sessions->JoinSession(*UserId, SessionName, SearchResult);
		}
	}

	return bSuccessful;
}

void UMultiplayerGI::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OnJoinSessionComplete %s, %d"), *SessionName.ToString(), static_cast<int32>(Result)));

	// Get the OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get SessionInterface from the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			// Clear the Delegate again
			Sessions->ClearOnJoinSessionCompleteDelegate_Handle(OnJoinSessionCompleteDelegateHandle);

			// Get the first local PlayerController, so we can call "ClientTravel" to get to the Server Map
			// This is something the Blueprint Node "Join Session" does automatically!
			APlayerController * const PlayerController = GetFirstLocalPlayerController();

			// We need a FString to use ClientTravel and we can let the SessionInterface contruct such a
			// String for us by giving him the SessionName and an empty String. We want to do this, because
			// Every OnlineSubsystem uses different TravelURLs
			FString TravelURL;

			if (PlayerController && Sessions->GetResolvedConnectString(SessionName, TravelURL))
			{
				// Finally call the ClienTravel. If you want, you could print the TravelURL to see
				// how it really looks like
				PlayerController->ClientTravel(TravelURL, ETravelType::TRAVEL_Absolute);
			}
		}
	}
}

void UMultiplayerGI::OnDestroySessionComplete(FName SessionName, bool bWasSuccessful)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OnDestroySessionComplete %s, %d"), *SessionName.ToString(), bWasSuccessful));

	// Get the OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get the SessionInterface from the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			// Clear the Delegate
			Sessions->ClearOnDestroySessionCompleteDelegate_Handle(OnDestroySessionCompleteDelegateHandle);

			// If it was successful, we just load another level (could be a MainMenu!)
			//TODO
			//bool is allways false, why=????
			if (bWasSuccessful)
			{
				LobbyRef->RemoveFromParent();
				ShowMainMenu();
				//TODO set opened map to mainmenu, might have to be created
				//UGameplayStatics::OpenLevel(GetWorld(), "Menu", true);
			}
		}
	}
}

void UMultiplayerGI::CreateSession(FName SessionName, bool bIsLAN, int32 MaxNumPlayers, FName MapName)
{
	ShowLoadingScreen();
	// Creating a local player where we can get the UserID from
	ULocalPlayer* const Player = GetFirstGamePlayer();

	// Call our custom HostSession function. GameSessionName is a GameInstance variable
	//HostSession(Player->GetPreferredUniqueNetId(), GameSessionName, true, true, 4);
	//AGameModeBase* gm = UGameplayStatics::GetGameMode(GetWorld());
	//ALobbyGameMode* agm = Cast<ALobbyGameMode>(gm);
	////GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, agm->GetName());
	//FPlayerInfo info;
	//info.playerName = "jony";
	//agm->players.Add(info);
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, agm->players[0].playerName);
	UE_LOG(LobbyLog, Error, TEXT("playername: %s"), *LoadGame("PlayerSettingsSave").playerName);
	HostSession(Player->GetPreferredUniqueNetId(), SessionName, bIsLAN, true, MaxNumPlayers, MapName);
	UOnlineEngineInterface::Get()->RegisterPlayer(GetWorld(), SessionName, *Player->GetPreferredUniqueNetId(), false);
}

void  UMultiplayerGI::StartSession()
{
	//ShowLoadingScreen();
	//TODO find way to keep the gamemode after level start
	//AGameModeBase* gm = UGameplayStatics::GetGameMode(GetWorld());
	//ALobbyGameMode* agm = Cast<ALobbyGameMode>(gm);
	////GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, agm->GetName());
	//for (int i = 0; i < agm->players.Num(); i++)
	//{
	//	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, agm->players[i].playerName);
	//}

	//TODO load map and let all players travel there
	//maybe change from setting_mapname to parameter for selecting map in lobby -> need variable in lobby widget
	FString mapString;
	SessionSettings->Get<FString>(SETTING_MAPNAME, mapString);
	FName mapName = FName(*mapString);
	GetWorld()->ServerTravel("Area51");
}

void UMultiplayerGI::FindOnlineGames(bool bIsLAN, TArray<FBlueprintSessionResult> &results)
{
	ULocalPlayer* const Player = GetFirstGamePlayer();
	temp_results.Empty();
	currentState = EFindState::Default;

	//FindSessions(Player->GetPreferredUniqueNetId(), true, true);
	FindSessions(Player->GetPreferredUniqueNetId(), bIsLAN, true);
	if (currentState == EFindState::OnSuccess)
	{

		FString gameName;
		temp_results[0].OnlineResult.Session.SessionSettings.Get<FString>(FName("GameName"), gameName);
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Result: %s "), *gameName));
		results = temp_results;
		FString gameName2;
		results[0].OnlineResult.Session.SessionSettings.Get<FString>(FName("GameName"), gameName2);
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Result2: %s "), *gameName2));

	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Fail")));
	}
}

void UMultiplayerGI::JoinOnlineGame(FBlueprintSessionResult result)
{
	ShowLoadingScreen();
	ULocalPlayer* const Player = GetFirstGamePlayer();

	// Just a SearchResult where we can save the one we want to use, for the case we find more than one!
	FOnlineSessionSearchResult SearchResult;
	SearchResult = result.OnlineResult;

	// To avoid something crazy, we filter sessions from ourself
	/*if (SessionSearch->SearchResults[i].Session.OwningUserId != Player->GetPreferredUniqueNetId())
	{
	SearchResult = SessionSearch->SearchResults[i];

	// Once we found sounce a Session that is not ours, just join it. Instead of using a for loop, you could
	// use a widget where you click on and have a reference for the GameSession it represents which you can use
	// here
	JoinSession(Player->GetPreferredUniqueNetId(), GameSessionName, SearchResult);
	break;
	}*/
	JoinSession(SearchResult.Session.OwningUserId, GameSessionName, SearchResult);
}

void UMultiplayerGI::DestroySessionAndLeaveGame()
{
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			Sessions->AddOnDestroySessionCompleteDelegate_Handle(OnDestroySessionCompleteDelegate);

			//TODO destroysession is called but doesnt work/change map
			ULocalPlayer* const Player = GetFirstGamePlayer();
			UOnlineEngineInterface::Get()->UnregisterPlayer(GetWorld(), GameSessionName, *Player->GetPreferredUniqueNetId());
			Sessions->DestroySession(GameSessionName);
		}
	}
}

void UMultiplayerGI::ShowMainMenu()
{
	UE_LOG(LobbyLog, Error, TEXT("blah: %i"), 1234);
	if (!IsValid(MainMenuRef))
	{
		//create mainmenu widget based on wMainMenu
		MainMenuRef = CreateWidget<UUserWidget>(GetWorld(), wMainMenu);
	}
	MainMenuRef->AddToViewport();
	EnableMouse();
}

void UMultiplayerGI::ShowHostMenu()
{
	if (!IsValid(HostMenuRef))
	{
		//create hostmenu widget based on wHostMenu
		HostMenuRef = CreateWidget<UUserWidget>(GetWorld(), wHostMenu);
	}
	HostMenuRef->AddToViewport();
}

void UMultiplayerGI::ShowServerMenu()
{
	if (!IsValid(ServerMenuRef))
	{
		//create servermenu  based on wServerMenu
		ServerMenuRef = CreateWidget<UUserWidget>(GetWorld(), wServerMenu);
	}
	ServerMenuRef->AddToViewport();
}

void UMultiplayerGI::ShowLoadingScreen()
{
	if (!wLoadingScreen)
	{
		return;
	}
	if (!IsValid(LoadingScreenRef))
	{
		//create loadingScreen widget based on wLoadingScreen
		LoadingScreenRef = CreateWidget<UUserWidget>(GetWorld(), wLoadingScreen);
	}
	LoadingScreenRef->AddToViewport();
}

void UMultiplayerGI::ShowOptions()
{
	if (!IsValid(OptionsRef))
	{
		OptionsRef = CreateWidget<UUserWidget>(GetWorld(), wOptions);
	}
	OptionsRef->AddToViewport();
}

void UMultiplayerGI::ShowPlayerSettings()
{
	if (!IsValid(PlayerSettingsRef))
	{
		PlayerSettingsRef = CreateWidget<UUserWidget>(GetWorld(), wPlayerSettings);
	}
	PlayerSettingsRef->AddToViewport();
	EnableMouse();
}

void UMultiplayerGI::ShowLobby()
{
	if (!IsValid(LobbyRef))
	{
		LobbyRef = CreateWidget<UUserWidget>(GetWorld(), wLobby);
	}
	LobbyRef->AddToViewport();
	EnableMouse();
}

void UMultiplayerGI::EnableMouse()
{
	APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	if (PC)
	{
		PC->bShowMouseCursor = true;
		PC->bEnableClickEvents = true;
		PC->bEnableMouseOverEvents = true;
	}
}

int32 UMultiplayerGI::GetPingInMs(const FBlueprintSessionResult& Result)
{
	return Result.OnlineResult.PingInMs;
}

FString UMultiplayerGI::GetServerName(const FBlueprintSessionResult& Result)
{
	FString gameName;
	Result.OnlineResult.Session.SessionSettings.Get<FString>(FName("GameName"), gameName);
	return gameName;
}

int32 UMultiplayerGI::GetCurrentPlayers(const FBlueprintSessionResult& Result)
{
	return Result.OnlineResult.Session.SessionSettings.NumPublicConnections - Result.OnlineResult.Session.NumOpenPublicConnections;
}

int32 UMultiplayerGI::GetMaxPlayers(const FBlueprintSessionResult& Result)
{
	return Result.OnlineResult.Session.SessionSettings.NumPublicConnections;
}

UTexture2D* UMultiplayerGI::ChangeAvatar(int& currentID, int changeVal, TArray<UTexture2D*> avatars)
{
	int length = avatars.Num();
	if (currentID + changeVal < 0)
	{
		currentID = length - 1;
	}
	else
	{
		currentID = (currentID + changeVal) % length;
	}

	UTexture2D* val = avatars[currentID];
	return val;
}

void UMultiplayerGI::SaveGame(FString saveGameSlotName/*, TSubclassOf<USaveGame> SaveGameClass*/, FPlayerInfo playerInfo)
{
	UMultiplayerSaveGame* saveGameRef = Cast<UMultiplayerSaveGame>(UGameplayStatics::CreateSaveGameObject(UMultiplayerSaveGame::StaticClass()));
	saveGameRef->S_PlayerInfo = playerInfo;

	UGameplayStatics::SaveGameToSlot(saveGameRef, saveGameSlotName, 0);
}

FPlayerInfo UMultiplayerGI::LoadGame(FString saveGameSlotName)
{
	UMultiplayerSaveGame* saveGameRef = Cast<UMultiplayerSaveGame>(UGameplayStatics::CreateSaveGameObject(UMultiplayerSaveGame::StaticClass()));
	saveGameRef = Cast<UMultiplayerSaveGame>(UGameplayStatics::LoadGameFromSlot(saveGameSlotName, 0));
	FPlayerInfo val = saveGameRef->S_PlayerInfo;
	return val;
}

bool UMultiplayerGI::SaveGameCheck(FString saveGameSlotName)
{
	return UGameplayStatics::DoesSaveGameExist(saveGameSlotName, 0);
}

FString UMultiplayerGI::GetSteamOrComputerName()
{
	//TODO
	//const FString* val = *FGenericPlatformProcess::UserName();
	FString val = "John DOE was here and has still some work to do!";
	return val;
}


void UMultiplayerGI::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to every client, no special condition required
	DOREPLIFETIME(UMultiplayerGI, allPlayerController);
}