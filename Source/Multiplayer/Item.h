// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types.h"
#include "Sound/SoundCue.h"
#include "Item.generated.h"

UENUM()
enum class EItemState
{
	Idle,
	Active,
	Equipping,
	Reloading
};

UCLASS(ABSTRACT, Blueprintable)
class MULTIPLAYER_API AItem : public AActor
{
	GENERATED_UCLASS_BODY()
	
		virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:

	void OnEquip();

	virtual void OnUnEquip();

	virtual void OnEquipFinished();

	virtual void OnEnterInventory(AMultiplayerCharacter* NewOwner);

	virtual void OnLeaveInventory();

	bool IsEquipped() const;

	bool IsAttachedToPawn() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Item")
		USkeletalMeshComponent* GetItemMesh() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Item")
		class AMultiplayerCharacter* GetPawnOwner() const;

	void SetOwningPawn(AMultiplayerCharacter* NewOwner);

	float GetEquipStartedTime() const;

	float GetEquipDuration() const;

	float EquipStartedTime;

	float EquipDuration;

	bool bIsEquipped;

	bool bPendingEquip;

	//FTimerHandle HandleFiringTimerHandle;

	FTimerHandle EquipFinishedTimerHandle;

	UPROPERTY(EditDefaultsOnly, Category = "Game|Item")
		TSubclassOf<class AItemPickup> ItemPickupClass;

	/* The character socket to store this item at. */
	EInventorySlot StorageSlot;

protected:

	UPROPERTY(Transient, ReplicatedUsing = OnRep_MyPawn)
		class AMultiplayerCharacter* MyPawn;

	/** item mesh: 3rd person view */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		USkeletalMeshComponent* Mesh;

	UFUNCTION()
		void OnRep_MyPawn();

	void AttachMeshToPawn(EInventorySlot Slot = EInventorySlot::Secondary);

	void DetachMeshFromPawn();

public:

	FORCEINLINE EInventorySlot GetStorageSlot()
	{
		return StorageSlot;
	}

	/************************************************************************/
	/* Fire & Damage Handling                                               */
	/************************************************************************/

public:
	EItemState GetCurrentState() const;

protected:

	bool CanBeUsed() const;

	UPROPERTY(EditDefaultsOnly)
		float TimeBetweenUse;

private:

	void SetItemState(EItemState NewState);

	void DetermineItemState();

	EItemState CurrentState;

	//bool bRefiring;

	//float LastFireTime;

	/************************************************************************/
	/* Simulation & FX                                                      */
	/************************************************************************/

private:

	UFUNCTION()
		void OnRep_BurstCounter();

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundCue* ActiveSound;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundCue* EquipSound;

	UPROPERTY(EditDefaultsOnly)
		UAnimMontage* EquipAnim;

	UPROPERTY(EditDefaultsOnly)
		UAnimMontage* ActiveAnim;

	bool bPlayingActiveAnim;

	//TODO maybe change to using time or calculated use counte based on costs
	UPROPERTY(Transient, ReplicatedUsing = OnRep_BurstCounter)
		int32 BurstCounter;

protected:

	virtual void SimulateWeaponFire();

	virtual void StopSimulatingWeaponFire();

	UAudioComponent* PlayItemSound(USoundCue* SoundToPlay);

	float PlayItemAnimation(UAnimMontage* Animation, float InPlayRate = 1.f, FName StartSectionName = NAME_None);

	void StopItemAnimation(UAnimMontage* Animation);
};
