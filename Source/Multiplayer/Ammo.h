// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/ArrowComponent.h"
#include "Ammo.generated.h"

UCLASS()
class MULTIPLAYER_API AAmmo : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAmmo(const class FObjectInitializer& PCIP);

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		UParticleSystemComponent* Emitter;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		UArrowComponent* BulletHeadPoint;

protected:
	virtual void BeginPlay() override;

	float TotalDamage;

	float TotalRange;

	float TotalAccuracy;

	UPROPERTY(EditDefaultsOnly)
		float HitDamageModifier;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class UDamageType> DamageType;

	UPROPERTY(EditDefaultsOnly)
		float WeaponRangeModifier;

	/*FieldofView value for cameracomp of character*/
	UPROPERTY(EditDefaultsOnly)
		float WeaponAccuracyModifier;

	/*may be positive and negative, weapon prevents from dropping below 0*/
	UPROPERTY(EditDefaultsOnly)
		float WeaponShotAmountModifier;

	UPROPERTY(EditDefaultsOnly)
		float WeaponShotScatterModifier;

	/* Hit verification: threshold for dot product between view direction and hit direction */
	UPROPERTY(EditDefaultsOnly)
		float AllowedViewDotHitDir;

	/* Hit verification: scale for bounding box of hit actor */
	UPROPERTY(EditDefaultsOnly)
		float ClientSideHitLeeway;

	FHitResult HitTrace(const FVector& TraceFrom, const FVector& TraceTo) const;

	void DealDamage(const FHitResult& Impact, const FVector& ShootDir);

	bool ShouldDealDamage(AActor* TestActor) const;

	void ProcessInstantHit(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir);

	void ProcessInstantHitConfirmed(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir);

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerNotifyHit(const FHitResult& Impact, FVector_NetQuantizeNormal ShootDir);

	UPROPERTY(Transient, ReplicatedUsing = OnRep_HitLocation)
		FVector HitOriginNotify;

	UFUNCTION()
		void OnRep_HitLocation();

private:
	void SimulateInstantHit(const FVector& Origin);

	void SpawnImpactEffects(const FHitResult& Impact);

	/* Particle FX played when a surface is hit. */
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class AImpactEffect> ImpactTemplate;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_MyPawn)
		class AMultiplayerCharacter* MyPawn;

public:	
	FVector GetBulletHeadLocation() const;

	void SetOwningPawn(AMultiplayerCharacter* NewOwner);

	UFUNCTION()
		void OnRep_MyPawn();

	virtual void SetValues();

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerSetValues();

	void SetMyPawn(AMultiplayerCharacter* NewPawn);

	float GetScatterAmount();

	float GetShotAmount();
};
