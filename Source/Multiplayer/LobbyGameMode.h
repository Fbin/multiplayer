// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Types.h"
#include "PCMultiplayer.h"
#include "LobbyPlayerController.h"
#include "LobbyGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYER_API ALobbyGameMode : public AGameMode
{
	GENERATED_BODY()

		ALobbyGameMode(const FObjectInitializer& ObjectInitializer);
	
public:
	UPROPERTY(Replicated)
		TArray<FPlayerInfo> players;

	UPROPERTY(Replicated)
		int currentPlayerAmount;

	virtual void PostLogin(APlayerController* NewPlayer) override;
	
	UFUNCTION(BlueprintCallable)
		void SetPlayerCharacter(UClass* selectedCharacter, int index);

	UFUNCTION(BlueprintCallable)
		void SetPlayerState(bool isReady, int index);

	UFUNCTION(BlueprintCallable)
		void SetPlayerWeapon(AWeapon* selectedWeapon, int index);

	UFUNCTION(BlueprintCallable)
		void SetPlayerItem(AItem* selectedItem, int index);

	void SwapCharacter(APlayerController* playerController, TSubclassOf<ACharacter> newCharacter, bool changedStatus);

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation)
		void ServerSwapCharacter(APlayerController* playerController, TSubclassOf<ACharacter> newCharacter, bool changedStatus);

	void UpdateAll();

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation)
		void ServerUpdateAll();

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation)
		void ServerUpdateGameSettings(UTexture2D* mapImage, const FText& mapName, int mapId);

	void AddToKickList();

	UFUNCTION(BlueprintCallable, Reliable, Server, WithValidation)
		void RespawnPlayer(APlayerController* pcRef);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool CanStart;

	UPROPERTY(EditDefaultsOnly, Category = "Defaults")
		TSubclassOf<AActor> startPointClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> startPoints;

	UPROPERTY(Replicated, EditDefaultsOnly)
		UTexture2D* gMMapImage;

	UPROPERTY(Replicated, EditDefaultsOnly)
		FText gMMapName;

	UPROPERTY(Replicated)
		int gMMapId;
};
