// Fill out your copyright notice in the Description page of Project Settings.

#include "MultiplayerCharacter.h"
#include "Multiplayer.h"
#include "UsableActor.h"
#include "MultiplayerCharacterMoveComp.h"
#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "Animation/AnimationAsset.h"
#include "Weapon.h"
#include "WeaponActor.h"
#include "WeaponPickup.h"
#include "Item.h"
#include "ItemPickup.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/GameMode.h"
#include "Ammo.h"


// Sets default values
AMultiplayerCharacter::AMultiplayerCharacter(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UMultiplayerCharacterMoveComp>(ACharacter::CharacterMovementComponentName))
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	UCharacterMovementComponent* moveComp = GetCharacterMovement();
	moveComp->GravityScale = 1.5f;
	moveComp->JumpZVelocity = 620;
	moveComp->bCanWalkOffLedgesWhenCrouching = true;
	moveComp->MaxWalkSpeedCrouched = 200;
	moveComp->GetNavAgentPropertiesRef().bCanCrouch = true;

	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	
	health = 100;
	MaxUseDistance = 800;
	trhowStrength = 1000;
	bHasNewFocus = true;
	TargetingSpeedModifier = 0.5f;
	SprintingSpeedModifier = 2.5f;
	
	CameraComp = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("Camera"));
	CameraComp->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	CameraComp->bUsePawnControlRotation = true;

	bIsCarrying = false;
	GetMesh()->bGenerateOverlapEvents = true;
}

// Called when the game starts or when spawned
void AMultiplayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	if (Role == ROLE_Authority)
	{

	}
}

void AMultiplayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bWantsToRun && !IsSprinting())
	{
		SetSprinting(true);
	}

	if (bIsTargeting)
	{
		if (CurrentWeapon != nullptr)
		{
			CameraComp->FieldOfView = Cast<AWeaponActor>(CurrentWeapon)->WeaponZoom;
		}
		else
		{
			CameraComp->FieldOfView = 60;
		}
	}
	else
	{
		CameraComp->FieldOfView = 90;
	}

	if (Controller && Controller->IsLocalController())
	{
		AUsableActor* Usable = GetUsableInView();

		// End Focus
		if (FocusedUsableActor != Usable)
		{
			if (FocusedUsableActor)
			{
				FocusedUsableActor->OnEndFocus();
			}

			bHasNewFocus = true;
		}

		// Assign new Focus
		FocusedUsableActor = Usable;

		// Start Focus.
		if (Usable)
		{
			if (bHasNewFocus)
			{
				Usable->OnBeginFocus();
				bHasNewFocus = false;
			}
		}

		if (bIsCarrying)
		{
			FVector CamLoc;
			FRotator CamRot;

			Controller->GetPlayerViewPoint(CamLoc, CamRot);
			FVector2D dir = FVector2D(carryiedObject->GetActorLocation().X - GetActorLocation().X, carryiedObject->GetActorLocation().Y - GetActorLocation().Y);
			float newZ = FMath::Clamp(dir.Size()*FMath::Tan(FMath::DegreesToRadians(CamRot.Pitch)) + CamLoc.Z, 0.f, MaxUseDistance);
			carryiedObject->SetActorLocation(FVector(carryiedObject->GetActorLocation().X, carryiedObject->GetActorLocation().Y, newZ));
		}
	}
}

void AMultiplayerCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	DestroyInventory();
}

//bind functionality to input
void AMultiplayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	/* Movement */
	InputComponent->BindAxis("MoveForward", this, &AMultiplayerCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AMultiplayerCharacter::MoveRight);

	InputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	InputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	InputComponent->BindAction("Jump", IE_Pressed, this, &AMultiplayerCharacter::OnJumpStart);
	InputComponent->BindAction("Jump", IE_Released, this, &AMultiplayerCharacter::OnJumpEnd);
	InputComponent->BindAction("SprintHold", IE_Pressed, this, &AMultiplayerCharacter::OnSprintStart);
	InputComponent->BindAction("SprintHold", IE_Released, this, &AMultiplayerCharacter::OnSprintEnd);
	InputComponent->BindAction("CrouchToggle", IE_Released, this, &AMultiplayerCharacter::OnCrouchToggle);

	// Interaction
	InputComponent->BindAction("Use", IE_Pressed, this, &AMultiplayerCharacter::Use);
	InputComponent->BindAction("UseItem", IE_Pressed, this, &AMultiplayerCharacter::UseItem);
	InputComponent->BindAction("DropWeapon", IE_Pressed, this, &AMultiplayerCharacter::DropWeapon);
	InputComponent->BindAction("DropItem", IE_Pressed, this, &AMultiplayerCharacter::DropItem);

	// Weapons
	InputComponent->BindAction("Targeting", IE_Pressed, this, &AMultiplayerCharacter::OnStartTargeting);
	InputComponent->BindAction("Targeting", IE_Released, this, &AMultiplayerCharacter::OnEndTargeting);
	InputComponent->BindAction("Fire", IE_Pressed, this, &AMultiplayerCharacter::OnStartFire);
	InputComponent->BindAction("Fire", IE_Released, this, &AMultiplayerCharacter::OnStopFire);
}

void AMultiplayerCharacter::MoveForward(float Val)
{
	if (Controller && Val != 0.f)
	{
		// Limit pitch when walking or falling
		const bool bLimitRotation = (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling());
		const FRotator Rotation = bLimitRotation ? GetActorRotation() : Controller->GetControlRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);

		AddMovementInput(Direction, Val);
	}
}


void AMultiplayerCharacter::MoveRight(float Val)
{
	if (Val != 0.f)
	{
		const FRotator Rotation = GetActorRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, Val);
		//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::SanitizeFloat(Val));
	}
}

void AMultiplayerCharacter::OnJumpStart()
{
	bPressedJump = true;

	SetIsJumping(true);
}


void AMultiplayerCharacter::OnJumpEnd()
{
	bPressedJump = false;
}


bool AMultiplayerCharacter::IsInitiatedJump() const
{
	return bIsJumping;
}


void AMultiplayerCharacter::SetIsJumping(bool NewJumping)
{
	// Go to standing pose if trying to jump while crouched
	if (bIsCrouched && NewJumping)
	{
		UnCrouch();
	}
	else
	{
		bIsJumping = NewJumping;
	}

	if (Role < ROLE_Authority)
	{
		ServerSetIsJumping(NewJumping);
	}
}


//void AMultiplayerCharacter::OnLanded(const FHitResult& Hit)
//{
//	Super::OnLanded(Hit);
//
//	SetIsJumping(false);
//}


void AMultiplayerCharacter::OnMovementModeChanged(EMovementMode PrevMovementMode, uint8 PreviousCustomMode)
{
	Super::OnMovementModeChanged(PrevMovementMode, PreviousCustomMode);

	/* Check if we are no longer falling/jumping */
	if (PrevMovementMode == EMovementMode::MOVE_Falling &&
		GetCharacterMovement()->MovementMode != EMovementMode::MOVE_Falling)
	{
		SetIsJumping(false);
	}
}


void AMultiplayerCharacter::ServerSetIsJumping_Implementation(bool NewJumping)
{
	SetIsJumping(NewJumping);
}

bool AMultiplayerCharacter::ServerSetIsJumping_Validate(bool NewJumping)
{
	return true;
}

void AMultiplayerCharacter::SetSprinting(bool NewSprinting)
{
	bWantsToRun = NewSprinting;

	if (bIsCrouched)
		UnCrouch();

	// TODO: Stop weapon fire or decrease accuracy
	//CurrentWeapon->StopFire();

	if (Role < ROLE_Authority)
	{
		ServerSetSprinting(NewSprinting);
	}
}


void AMultiplayerCharacter::OnSprintStart()
{
	if (carryiedObject != nullptr)
	{
		carryiedObject->Drop();
		bIsCarrying = false;
		carryiedObject = nullptr;
	}
	SetSprinting(true);
}


void AMultiplayerCharacter::OnSprintEnd()
{
	SetSprinting(false);
}


void AMultiplayerCharacter::ServerSetSprinting_Implementation(bool NewSprinting)
{
	SetSprinting(NewSprinting);
}


bool AMultiplayerCharacter::ServerSetSprinting_Validate(bool NewSprinting)
{
	return true;
}


bool AMultiplayerCharacter::IsSprinting() const
{
	if (!GetCharacterMovement())
		return false;

	return bWantsToRun && !IsTargeting() && !GetVelocity().IsZero()
		&& (GetVelocity().GetSafeNormal2D() | GetActorRotation().Vector()) > 0.8; // Changing this value to 0.1 allows for diagonal sprinting. (holding W+A or W+D keys)
}


float AMultiplayerCharacter::GetSprintingSpeedModifier() const
{
	return SprintingSpeedModifier;
}

AUsableActor* AMultiplayerCharacter::GetUsableInView()
{
	FVector CamLoc;
	FRotator CamRot;

	if (Controller == NULL)
		return NULL;

	Controller->GetPlayerViewPoint(CamLoc, CamRot);
	const FVector TraceStart = CamLoc;
	const FVector Direction = CamRot.Vector();
	const FVector TraceEnd = TraceStart + (Direction * MaxUseDistance);

	FCollisionQueryParams TraceParams(FName(TEXT("TraceUsableActor")), true, this);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = false;
	TraceParams.bTraceComplex = true;

	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, ECC_Visibility, TraceParams);

	//DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, false, 1.0f);

	return Cast<AUsableActor>(Hit.GetActor());
}


void AMultiplayerCharacter::Use()
{
	if (Role == ROLE_Authority)
	{
		//TODO check if throw works with this right after
		AUsableActor* Usable = GetUsableInView();
		if (bIsCarrying)
		{
			carryiedObject->Throw();
			bIsCarrying = false;
			carryiedObject = nullptr;
			return;
		}
		if (Usable)
		{
			Usable->OnUsed(this);
			return;
		}
	}
	else
	{
		ServerUse();
	}
}


void AMultiplayerCharacter::ServerUse_Implementation()
{
	Use();
}


bool AMultiplayerCharacter::ServerUse_Validate()
{
	return true;
}


void AMultiplayerCharacter::UseItem()
{
	if (Role == ROLE_Authority)
	{
		if (Inventory.Num() > 0)
		{
			for (int32 i = 0; i < Inventory.Num(); i++)
			{
				/*if (Cast<AItem>(Inventory[i]))
				{
					EquipWeapon(Cast<AItem>(Inventory[i]));
					return;
				}*/
			}
		}
	}
	else
	{
		ServerUse();
	}
}


void AMultiplayerCharacter::ServerUseItem_Implementation()
{
	UseItem();
}


bool AMultiplayerCharacter::ServerUseItem_Validate()
{
	return true;
}


void AMultiplayerCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Value is already updated locally, skip in replication step
	DOREPLIFETIME_CONDITION(AMultiplayerCharacter, bWantsToRun, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AMultiplayerCharacter, bIsTargeting, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AMultiplayerCharacter, bIsJumping, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(AMultiplayerCharacter, bIsCarrying, COND_SkipOwner);

	// Replicate to every client, no special condition required
	DOREPLIFETIME(AMultiplayerCharacter, health);
	DOREPLIFETIME(AMultiplayerCharacter, LastTakeHitInfo);
	DOREPLIFETIME(AMultiplayerCharacter, CurrentWeapon);
	DOREPLIFETIME(AMultiplayerCharacter, CurrentItem);
}

void AMultiplayerCharacter::OnCrouchToggle()
{
	// If we are crouching then CanCrouch will return false. If we cannot crouch then calling Crouch() wont do anything
	if (CanCrouch())
	{
		Crouch();
	}
	else
	{
		UnCrouch();
	}
}

float AMultiplayerCharacter::GetHealth() const
{
	return health;
}

void AMultiplayerCharacter::GainHealth(float AmountRestored)
{
	health = FMath::Clamp(health + AmountRestored, 0.0f, GetMaxHealth());

	APlayerController* PC = Cast<APlayerController>(Controller);
	/*if (PC)
	{
		ASHUD* MyHUD = Cast<ASHUD>(PC->GetHUD());
		if (MyHUD)
		{
			MyHUD->MessageReceived("Food item consumed!");
		}
	}*/
}

float AMultiplayerCharacter::GetMaxHealth() const
{
	return GetClass()->GetDefaultObject<AMultiplayerCharacter>()->health;
}

bool AMultiplayerCharacter::IsAlive() const
{
	return health > 0;
}

float AMultiplayerCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	if (health <= 0.f)
	{
		return 0.f;
	}

	const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	if (ActualDamage > 0.f)
	{
		health -= ActualDamage;
		if (!IsAlive())
		{
			Die(ActualDamage, DamageEvent, EventInstigator, DamageCauser);
		}
		else
		{
			PlayHit(ActualDamage, DamageEvent, EventInstigator->GetPawn(), DamageCauser, false);
		}
	}

	return ActualDamage;
}

bool AMultiplayerCharacter::CanDie(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser) const
{
	if (bIsDying ||
		IsPendingKill() ||
		Role != ROLE_Authority ||
		GetWorld()->GetAuthGameMode() == NULL)
	{
		return false;
	}

	return true;
}


bool AMultiplayerCharacter::Die(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser)
{
	if (!CanDie(KillingDamage, DamageEvent, Killer, DamageCauser))
	{
		return false;
	}

	health = FMath::Min(0.0f, health);

	/* Fallback to default DamageType if none is specified */
	UDamageType const* const DamageType = DamageEvent.DamageTypeClass ? DamageEvent.DamageTypeClass->GetDefaultObject<UDamageType>() : GetDefault<UDamageType>();
	Killer = GetDamageInstigator(Killer, *DamageType);

	OnDeath(KillingDamage, DamageEvent, Killer ? Killer->GetPawn() : NULL, DamageCauser);
	return true;
}


void AMultiplayerCharacter::Suicide()
{
	KilledBy(this);
}


void AMultiplayerCharacter::KilledBy(class APawn* EventInstigator)
{
	if (Role == ROLE_Authority && !bIsDying)
	{
		AController* Killer = nullptr;
		if (EventInstigator != nullptr)
		{
			Killer = EventInstigator->Controller;
			LastHitBy = nullptr;
		}

		Die(health, FDamageEvent(UDamageType::StaticClass()), Killer, nullptr);
	}
}


void AMultiplayerCharacter::OnDeath(float KillingDamage, FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser)
{
	if (bIsDying)
	{
		return;
	}

	bReplicateMovement = false;
	bTearOff = true;
	bIsDying = true;

	PlayHit(KillingDamage, DamageEvent, PawnInstigator, DamageCauser, true);

	DestroyInventory();

	DetachFromControllerPendingDestroy();
	StopAllAnimMontages();

	/* Disable all collision on capsule */
	UCapsuleComponent* CapsuleComp = GetCapsuleComponent();
	/*CapsuleComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CapsuleComp->SetCollisionResponseToAllChannels(ECR_Ignore);*/

	USkeletalMeshComponent* Mesh3P = GetMesh();
	if (Mesh3P)
	{
		Mesh3P->SetCollisionProfileName(TEXT("Ragdoll"));
	}
	SetActorEnableCollision(true);

	SetRagdollPhysics();
}


void AMultiplayerCharacter::SetRagdollPhysics()
{
	bool bInRagdoll = false;
	USkeletalMeshComponent* Mesh3P = GetMesh();

	if (IsPendingKill())
	{
		bInRagdoll = false;
	}
	else if (!Mesh3P || !Mesh3P->GetPhysicsAsset())
	{
		bInRagdoll = false;
	}
	else
	{
		Mesh3P->SetAllBodiesSimulatePhysics(true);
		Mesh3P->SetSimulatePhysics(true);
		Mesh3P->WakeAllRigidBodies();
		Mesh3P->bBlendPhysics = true;

		bInRagdoll = true;
	}

	UCharacterMovementComponent* CharacterComp = Cast<UCharacterMovementComponent>(GetMovementComponent());
	if (CharacterComp)
	{
		CharacterComp->StopMovementImmediately();
		CharacterComp->DisableMovement();
		CharacterComp->SetComponentTickEnabled(false);
	}

	if (!bInRagdoll)
	{
		TurnOff();
		SetActorHiddenInGame(true);
		SetLifeSpan(1.0f);
	}
	else
	{
		SetLifeSpan(10.0f);
	}
}


void AMultiplayerCharacter::FellOutOfWorld(const class UDamageType& DmgType)
{
	Die(health, FDamageEvent(DmgType.GetClass()), NULL, NULL);
}


void AMultiplayerCharacter::PlayHit(float DamageTaken, struct FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser, bool bKilled)
{
	if (Role == ROLE_Authority)
	{
		ReplicateHit(DamageTaken, DamageEvent, PawnInstigator, DamageCauser, bKilled);
	}

	/* Apply damage momentum specific to the DamageType */
	if (DamageTaken > 0.f)
	{
		ApplyDamageMomentum(DamageTaken, DamageEvent, PawnInstigator, DamageCauser);
	}
}


void AMultiplayerCharacter::ReplicateHit(float DamageTaken, struct FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser, bool bKilled)
{
	const float TimeoutTime = GetWorld()->GetTimeSeconds() + 0.5f;

	FDamageEvent const& LastDamageEvent = LastTakeHitInfo.GetDamageEvent();
	if (PawnInstigator == LastTakeHitInfo.PawnInstigator.Get() && LastDamageEvent.DamageTypeClass == LastTakeHitInfo.DamageTypeClass)
	{
		// Same frame damage
		if (bKilled && LastTakeHitInfo.bKilled)
		{
			// Redundant death take hit, ignore it
			return;
		}

		DamageTaken += LastTakeHitInfo.ActualDamage;
	}

	LastTakeHitInfo.ActualDamage = DamageTaken;
	LastTakeHitInfo.PawnInstigator = PawnInstigator;
	LastTakeHitInfo.DamageCauser = DamageCauser;
	LastTakeHitInfo.SetDamageEvent(DamageEvent);
	LastTakeHitInfo.bKilled = bKilled;
	LastTakeHitInfo.EnsureReplication();
}

void AMultiplayerCharacter::OnRep_LastTakeHitInfo()
{
	if (LastTakeHitInfo.bKilled)
	{
		OnDeath(LastTakeHitInfo.ActualDamage, LastTakeHitInfo.GetDamageEvent(), LastTakeHitInfo.PawnInstigator.Get(), LastTakeHitInfo.DamageCauser.Get());
	}
	else
	{
		PlayHit(LastTakeHitInfo.ActualDamage, LastTakeHitInfo.GetDamageEvent(), LastTakeHitInfo.PawnInstigator.Get(), LastTakeHitInfo.DamageCauser.Get(), LastTakeHitInfo.bKilled);
	}
}


bool AMultiplayerCharacter::CanFire() const
{
	/* Add your own checks here, for example non-shooting areas or checking if player is in an NPC dialogue etc. */
	return IsAlive();
}


bool AMultiplayerCharacter::CanReload() const
{
	return IsAlive();
}


bool AMultiplayerCharacter::IsFiring() const
{
	return CurrentWeapon && CurrentWeapon->GetCurrentState() == EWeaponState::Firing;
}


FName AMultiplayerCharacter::GetInventoryAttachPoint(EInventorySlot Slot) const
{
	/* Return the socket name for the specified storage slot */
	switch (Slot)
	{
	case EInventorySlot::Hands:
		return WeaponAttachPoint;
	case EInventorySlot::Primary:
		return SpineAttachPoint;
	case EInventorySlot::Secondary:
		return PelvisAttachPoint;
	default:
		// Not implemented.
		return "";
	}
}


void AMultiplayerCharacter::SpawnDefaultInventory()
{
	if (Role < ROLE_Authority)
	{
		return;
	}

	for (int32 i = 0; i < DefaultInventoryClasses.Num(); i++)
	{
		if (DefaultInventoryClasses[i])
		{
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.bNoFail = true;
			
			if (Cast<AWeapon>(Inventory[i]))
			{
				AWeapon* NewWeapon = GetWorld()->SpawnActor<AWeapon>(DefaultInventoryClasses[i], SpawnInfo);

				AddWeapon(NewWeapon);
			}

			if (Cast<AItem>(Inventory[i]))
			{
				AItem* NewItem = GetWorld()->SpawnActor<AItem>(DefaultInventoryClasses[i], SpawnInfo);

				AddItem(NewItem);
			}
		}
	}

	// Equip first weapon in inventory
	if (Inventory.Num() > 0)
	{
		for (int32 i = 0; i < Inventory.Num(); i++)
		{
			if (Cast<AWeapon>(Inventory[i]))
			{
				EquipWeapon(Cast<AWeapon>(Inventory[i]));
				return;
			}
		}
	}
}


void AMultiplayerCharacter::DestroyInventory()
{
	if (Role < ROLE_Authority)
	{
		return;
	}

	for (int32 i = Inventory.Num() - 1; i >= 0; i--)
	{
		AWeapon* Weapon = Cast<AWeapon>(Inventory[i]);
		if (Weapon)
		{
			RemoveWeapon(Weapon);
			Weapon->Destroy();
			continue;
		}

		AItem* Item = Cast<AItem>(Inventory[i]);
		if (Item)
		{
			RemoveItem(Item);
			Item->Destroy();
		}
	}
}


void AMultiplayerCharacter::SetCurrentWeapon(class AWeapon* NewWeapon, bool bPlayAnimation, class AWeapon* LastWeapon)
{
	AWeapon* LocalLastWeapon = nullptr;

	if (LastWeapon)
	{
		LocalLastWeapon = LastWeapon;
	}
	else if (NewWeapon != CurrentWeapon)
	{
		LocalLastWeapon = CurrentWeapon;
	}

	// UnEquip the current
	if (LocalLastWeapon)
	{
		LocalLastWeapon->OnUnEquip();
	}

	CurrentWeapon = NewWeapon;

	if (NewWeapon)
	{
		NewWeapon->SetOwningPawn(this);
		NewWeapon->OnEquip(bPlayAnimation);
	}
}


void AMultiplayerCharacter::SetCurrentItem(AItem* newItem, bool bPlayAnimation, AItem* lastItem)
{
	AItem* LocalLastItem = nullptr;

	if (lastItem)
	{
		LocalLastItem = lastItem;
	}
	else if (newItem != CurrentItem)
	{
		LocalLastItem = CurrentItem;
	}

	// UnEquip the current
	if (LocalLastItem)
	{
		LocalLastItem->OnUnEquip();
	}

	CurrentItem = newItem;

	if (newItem)
	{
		newItem->SetOwningPawn(this);
		newItem->OnEquip(/*bPlayAnimation*/);
	}
}


void AMultiplayerCharacter::SetCurrentAmmo(AAmmo* newAmmo, AAmmo* lastAmmo)
{
	/*AAmmo* LocalLastAmmo = nullptr;

	if (lastAmmo)
	{
		LocalLastAmmo = lastAmmo;
	}
	else if (newAmmo != CurrentAmmo)
	{
		LocalLastAmmo = CurrentAmmo;
	}*/

	// UnEquip the current
	/*if (LocalLastAmmo)
	{
		LocalLastAmmo->OnUnEquip();
	}*/

	CurrentAmmo = newAmmo;

	if (newAmmo)
	{
		newAmmo->SetOwningPawn(this);
		//newAmmo->OnEquip(/*bPlayAnimation*/);
	}
}


void AMultiplayerCharacter::OnRep_CurrentWeapon(AWeapon* LastWeapon)
{
	//TODO bool to set or default?
	SetCurrentWeapon(CurrentWeapon, true, LastWeapon);
}


void AMultiplayerCharacter::OnRep_CurrentItem(AItem* LastItem)
{
	//TODO bool to set or default?
	SetCurrentItem(CurrentItem, true, LastItem);
}


void AMultiplayerCharacter::OnRep_CurrentAmmo(AAmmo* LastAmmo)
{
	//TODO bool to set or default?
	SetCurrentAmmo(CurrentAmmo, LastAmmo);
}


void AMultiplayerCharacter::EquipWeapon(AWeapon* Weapon)
{
	if (Weapon)
	{
		if (Role == ROLE_Authority)
		{
			//TODO bool to set or default?
			SetCurrentWeapon(Weapon, true);
		}
		else
		{
			ServerEquipWeapon(Weapon);
		}
	}
}


bool AMultiplayerCharacter::ServerEquipWeapon_Validate(AWeapon* Weapon)
{
	return true;
}


void AMultiplayerCharacter::ServerEquipWeapon_Implementation(AWeapon* Weapon)
{
	EquipWeapon(Weapon);
}


void AMultiplayerCharacter::EquipItem(AItem* Item)
{
	if (Item)
	{
		if (Role == ROLE_Authority)
		{
			//TODO bool to set or default?
			SetCurrentItem(Item, true);
		}
		else
		{
			ServerEquipItem(Item);
		}
	}
}


bool AMultiplayerCharacter::ServerEquipItem_Validate(AItem* Item)
{
	return true;
}


void AMultiplayerCharacter::ServerEquipItem_Implementation(AItem* Item)
{
	EquipItem(Item);
}


void AMultiplayerCharacter::AddWeapon(class AWeapon* Weapon)
{
	if (Weapon && Role == ROLE_Authority)
	{
		Weapon->OnEnterInventory(this);
		Inventory.AddUnique(Weapon);
		AAmmo* c = ammo->GetDefaultObject<AAmmo>();
		Cast<AWeaponActor>(Weapon)->SetValues(c->GetShotAmount(), c->GetScatterAmount());

		// Equip first weapon in inventory
		if (Inventory.Num() > 0)
		{
			for (int32 i = 0; i < Inventory.Num(); i++)
			{
				if (Cast<AWeapon>(Inventory[i]))
				{
					EquipWeapon(Cast<AWeapon>(Inventory[i]));
					return;
				}
			}
		}
	}
}


void AMultiplayerCharacter::AddItem(class AItem* Item)
{
	if (Item && Role == ROLE_Authority)
	{
		Item->OnEnterInventory(this);
		Inventory.AddUnique(Item);

		//TODO
		//only needed if item has equiped and carry slot like weapon (weapons automaticaly to spine(carryslot) and the to hands with equip)
		// Equip first item in inventory
		/*if (Inventory.Num() > 0)
		{
			for (int32 i = 0; i < Inventory.Num(); i++)
			{
				if (Cast<AItem>(Inventory[i]))
				{
					EquipItem(Cast<AItem>(Inventory[i]));
					return;
				}
			}
		}*/
	}
}


bool AMultiplayerCharacter::WeaponSlotAvailable(EInventorySlot CheckSlot)
{
	for (int32 i = 0; i < Inventory.Num(); i++)
	{
		if (Cast<AWeapon>(Inventory[i]))
		{
			return false;
		}
	}
	return true;
	/* Special find function as alternative to looping the array and performing if statements
	the [=] prefix means "capture by value", other options include [] "capture nothing" and [&] "capture by reference" */
	//return nullptr == Inventory.FindByPredicate([=](AWeapon* W) { return W->GetStorageSlot() == CheckSlot; });
}


bool AMultiplayerCharacter::ItemSlotAvailable(EInventorySlot CheckSlot)
{
	for (int32 i = 0; i < Inventory.Num(); i++)
	{
		if (Cast<AItem>(Inventory[i]))
		{
			return false;
		}
	}
	return true;
}


void AMultiplayerCharacter::RemoveWeapon(class AWeapon* Weapon)
{
	if (Weapon && Role == ROLE_Authority)
	{
		bool bIsCurrent = CurrentWeapon == Weapon;
		if (Inventory.Contains(Weapon))
		{
			Weapon->OnLeaveInventory();
		}
		Inventory.RemoveSingle(Weapon);

		/* Replace weapon if we removed our current weapon */
		//if (bIsCurrent && Inventory.Num() > 0)
			//SetCurrentWeapon(Inventory[0]);

		/* Clear reference to weapon if we have no items left in inventory */
		if (Inventory.Num() == 0)
			//TODO bool to set or default?
			SetCurrentWeapon(nullptr, true);
	}
}


void AMultiplayerCharacter::RemoveItem(class AItem* Item)
{
	if (Item && Role == ROLE_Authority)
	{
		bool bIsCurrent = CurrentItem == Item;
		if (Inventory.Contains(Item))
		{
			Item->OnLeaveInventory();
		}
		Inventory.RemoveSingle(Item);

		/* Replace Item if we removed our current Item */
		//if (bIsCurrent && Inventory.Num() > 0)
			//SetCurrentItem(Inventory[0]);

		/* Clear reference to Item if we have no items left in inventory */
		if (Inventory.Num() == 0)
			//TODO bool to set or default?
			SetCurrentItem(nullptr, true);
	}
}


void AMultiplayerCharacter::PawnClientRestart()
{
	Super::PawnClientRestart();

	//TODO bool to set or default?
	SetCurrentWeapon(CurrentWeapon, true);
}


void AMultiplayerCharacter::OnStartFire()
{
	if (IsSprinting())
	{
		SetSprinting(false);
	}
	StartWeaponFire();
}


void AMultiplayerCharacter::OnStopFire()
{
	StopWeaponFire();
}


void AMultiplayerCharacter::StartWeaponFire()
{
	if (!bWantsToFire)
	{
		bWantsToFire = true;
		if (CurrentWeapon)
		{
			CurrentWeapon->StartFire();
		}
	}
}


void AMultiplayerCharacter::StopWeaponFire()
{
	if (bWantsToFire)
	{
		bWantsToFire = false;
		if (CurrentWeapon)
		{
			CurrentWeapon->StopFire();
		}
	}
}

void AMultiplayerCharacter::DropWeapon()
{
	if (Role < ROLE_Authority)
	{
		ServerDropWeapon();
		return;
	}

	if (CurrentWeapon)
	{
		FVector CamLoc;
		FRotator CamRot;

		if (Controller == nullptr)
			return;

		/* Find a location to drop the item, slightly in front of the player. */
		Controller->GetPlayerViewPoint(CamLoc, CamRot);
		const FVector Direction = CamRot.Vector();
		const FVector SpawnLocation = GetActorLocation() + (Direction * DropItemDistance);

		FActorSpawnParameters SpawnInfo;
		SpawnInfo.bNoFail = true;
		AWeaponPickup* NewWeaponPickup = GetWorld()->SpawnActor<AWeaponPickup>(CurrentWeapon->WeaponPickupClass, SpawnLocation, FRotator::ZeroRotator, SpawnInfo);

		RemoveWeapon(CurrentWeapon);
	}
}


void AMultiplayerCharacter::ServerDropWeapon_Implementation()
{
	DropWeapon();
}


bool AMultiplayerCharacter::ServerDropWeapon_Validate()
{
	return true;
}


void AMultiplayerCharacter::DropItem()
{
	if (Role < ROLE_Authority)
	{
		ServerDropItem();
		return;
	}

	if (CurrentItem)
	{
		FVector CamLoc;
		FRotator CamRot;

		if (Controller == nullptr)
			return;

		/* Find a location to drop the item, slightly in front of the player. */
		Controller->GetPlayerViewPoint(CamLoc, CamRot);
		const FVector Direction = CamRot.Vector();
		const FVector SpawnLocation = GetActorLocation() + (Direction * DropItemDistance);

		FActorSpawnParameters SpawnInfo;
		SpawnInfo.bNoFail = true;
		AItemPickup* NewItemPickup = GetWorld()->SpawnActor<AItemPickup>(CurrentItem->ItemPickupClass, SpawnLocation, FRotator::ZeroRotator, SpawnInfo);

		RemoveItem(CurrentItem);
	}
}


void AMultiplayerCharacter::ServerDropItem_Implementation()
{
	DropItem();
}


bool AMultiplayerCharacter::ServerDropItem_Validate()
{
	return true;
}


void AMultiplayerCharacter::StopAllAnimMontages()
{
	USkeletalMeshComponent* UseMesh = GetMesh();
	if (UseMesh && UseMesh->AnimScriptInstance)
	{
		UseMesh->AnimScriptInstance->Montage_Stop(0.0f);
	}
}

void AMultiplayerCharacter::OnStartTargeting()
{
	if (carryiedObject != nullptr)
	{
		carryiedObject->Drop();
		bIsCarrying = false;
		carryiedObject = nullptr;
	}
	SetTargeting(true);
}


void AMultiplayerCharacter::OnEndTargeting()
{
	SetTargeting(false);
}

void AMultiplayerCharacter::SetTargeting(bool NewTargeting)
{
	bIsTargeting = NewTargeting;

	if (Role < ROLE_Authority)
	{
		ServerSetTargeting(NewTargeting);
	}
}


void AMultiplayerCharacter::ServerSetTargeting_Implementation(bool NewTargeting)
{
	SetTargeting(NewTargeting);
}


bool AMultiplayerCharacter::ServerSetTargeting_Validate(bool NewTargeting)
{
	return true;
}



bool AMultiplayerCharacter::IsTargeting() const
{
	return bIsTargeting;
}


float AMultiplayerCharacter::GetTargetingSpeedModifier() const
{
	return TargetingSpeedModifier;
}