// Fill out your copyright notice in the Description page of Project Settings.

#include "Item.h"
#include "Multiplayer.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SkeletalMeshComponent.h"
#include "MultiplayerCharacter.h"


AItem::AItem(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{
	Mesh = PCIP.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("ItemMesh3P"));
	Mesh->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	Mesh->bReceivesDecals = true;
	Mesh->CastShadow = true;
	Mesh->SetCollisionObjectType(ECC_WorldDynamic);
	Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Mesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	Mesh->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	RootComponent = Mesh;

	bIsEquipped = false;
	CurrentState = EItemState::Idle;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
	SetRemoteRoleForBackwardsCompat(ROLE_SimulatedProxy);
	bReplicates = true;
	bNetUseOwnerRelevancy = true;

	StorageSlot = EInventorySlot::Secondary;
}


void AItem::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	DetachMeshFromPawn();
	StopSimulatingWeaponFire();
}


USkeletalMeshComponent* AItem::GetItemMesh() const
{
	return Mesh;
}


class AMultiplayerCharacter* AItem::GetPawnOwner() const
{
	return MyPawn;
}


void AItem::SetOwningPawn(AMultiplayerCharacter* NewOwner)
{
	if (MyPawn != NewOwner)
	{
		Instigator = NewOwner;
		MyPawn = NewOwner;
		// Net owner for RPC calls.
		SetOwner(NewOwner);
	}
}


void AItem::OnRep_MyPawn()
{
	if (MyPawn)
	{
		OnEnterInventory(MyPawn);
	}
	else
	{
		OnLeaveInventory();

	}
}


void AItem::AttachMeshToPawn(EInventorySlot Slot)
{
	if (MyPawn)
	{
		// Remove and hide
		DetachMeshFromPawn();

		USkeletalMeshComponent* PawnMesh = MyPawn->GetMesh();
		FName AttachPoint = MyPawn->GetInventoryAttachPoint(Slot);
		Mesh->SetHiddenInGame(false);
		Mesh->AttachToComponent(PawnMesh, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, false), AttachPoint);
	}
}


void AItem::DetachMeshFromPawn()
{
	Mesh->DetachFromComponent(FDetachmentTransformRules(EDetachmentRule::KeepRelative, true));
	Mesh->SetHiddenInGame(true);
}


void AItem::OnEquip()
{
	AttachMeshToPawn();

	bPendingEquip = true;
	DetermineItemState();

	float Duration = PlayItemAnimation(EquipAnim);
	if (Duration <= 0.0f)
	{
		// Failsafe
		Duration = 0.5f;
	}
	EquipStartedTime = GetWorld()->TimeSeconds;
	EquipDuration = Duration;

	GetWorldTimerManager().SetTimer(EquipFinishedTimerHandle, this, &AItem::OnEquipFinished, Duration, false);

	if (MyPawn && MyPawn->IsLocallyControlled())
	{
		PlayItemSound(EquipSound);
	}
}


void AItem::OnUnEquip()
{
	//AttachMeshToPawn(StorageSlot);
	bIsEquipped = false;
	//TODO create StopUse() for items with continious use
	//StopFire();

	if (bPendingEquip)
	{
		StopItemAnimation(EquipAnim);
		bPendingEquip = false;

		GetWorldTimerManager().ClearTimer(EquipFinishedTimerHandle);
	}

	DetermineItemState();
}


void AItem::OnEnterInventory(AMultiplayerCharacter* NewOwner)
{
	SetOwningPawn(NewOwner);
	AttachMeshToPawn(StorageSlot);
}


void AItem::OnLeaveInventory()
{
	if (Role == ROLE_Authority)
	{
		SetOwningPawn(nullptr);
	}

	if (IsAttachedToPawn())
	{
		OnUnEquip();
	}

	DetachMeshFromPawn();
}


bool AItem::IsEquipped() const
{
	return bIsEquipped;
}


bool AItem::IsAttachedToPawn() const
{
	return bIsEquipped || bPendingEquip;
}


//void AItem::StartFire()
//{
//	if (Role < ROLE_Authority)
//	{
//		ServerStartFire();
//	}
//
//	if (!bWantsToFire)
//	{
//		bWantsToFire = true;
//		DetermineItemState();
//	}
//}


//TODO create StopUse() for continious use items same as startuse
//void AItem::StopFire()
//{
//	if (Role < ROLE_Authority)
//	{
//		ServerStopFire();
//	}
//
//	if (bWantsToFire)
//	{
//		bWantsToFire = false;
//		DetermineItemState();
//	}
//}


//bool AItem::ServerStartFire_Validate()
//{
//	return true;
//}
//
//
//void AItem::ServerStartFire_Implementation()
//{
//	StartFire();
//}
//
//
//bool AItem::ServerStopFire_Validate()
//{
//	return true;
//}
//
//
//void AItem::ServerStopFire_Implementation()
//{
//	StopFire();
//}


//bool AItem::CanFire() const
//{
//	bool bPawnCanFire = MyPawn && MyPawn->CanFire();
//	bool bStateOK = CurrentState == EWeaponState::Idle || CurrentState == EWeaponState::Firing;
//	return bPawnCanFire && bStateOK;
//}
//
//
//FHitResult AItem::WeaponTrace(const FVector& TraceFrom, const FVector& TraceTo) const
//{
//	FCollisionQueryParams TraceParams(TEXT("WeaponTrace"), true, Instigator);
//	TraceParams.bTraceAsyncScene = true;
//	TraceParams.bReturnPhysicalMaterial = true;
//
//	FHitResult Hit(ForceInit);
//	//TODO include Collisions in defaults
//	//GetWorld()->LineTraceSingleByChannel(Hit, TraceFrom, TraceTo, COLLISION_WEAPON, TraceParams);
//
//	return Hit;
//}


//TODO might be needed for continious item use
//void AItem::HandleFiring()
//{
//	if (CanFire())
//	{
//		if (GetNetMode() != NM_DedicatedServer)
//		{
//			SimulateWeaponFire();
//		}
//
//		if (MyPawn && MyPawn->IsLocallyControlled())
//		{
//			FireWeapon();
//
//			// TODO: Consume Ammo
//
//			// Update firing FX on remote clients if this is called on server
//			BurstCounter++;
//		}
//	}
//	// TODO: End repeat fire if ammo runs dry
//
//	if (MyPawn && MyPawn->IsLocallyControlled())
//	{
//		if (Role < ROLE_Authority)
//		{
//			ServerHandleFiring();
//		}
//
//		// TODO: if weapon is automatic firing rifle -> Setup refiring timer. (calls HandleFiring()
//
//		bRefiring = (CurrentState == EWeaponState::Firing && TimeBetweenShots > 0.0f);
//		if (bRefiring)
//		{
//			GetWorldTimerManager().SetTimer(HandleFiringTimerHandle, this, &AItem::HandleFiring, TimeBetweenShots, false);
//		}
//	}
//
//	LastFireTime = GetWorld()->GetTimeSeconds();
//}


void AItem::SimulateWeaponFire()
{
	if (!bPlayingActiveAnim)
	{
		PlayItemAnimation(ActiveAnim);
		bPlayingActiveAnim = true;
	}

	PlayItemSound(ActiveSound);

	// 	ASPlayerController* PC = (MyPawn != nullptr) ? Cast<ASPlayerController>(MyPawn->Controller) : nullptr;
	// 	if (PC && PC->IsLocalController())
	// 	{
	// 		// TODO: Add camera roll oscillation
	// 	}
}


void AItem::StopSimulatingWeaponFire()
{
	if (bPlayingActiveAnim)
	{
		StopItemAnimation(ActiveAnim);
		bPlayingActiveAnim = false;
	}
}



void AItem::OnRep_BurstCounter()
{
	if (BurstCounter > 0)
	{
		SimulateWeaponFire();
	}
	else
	{
		StopSimulatingWeaponFire();
	}
}


//TODO chained to handlefiring()
//bool AItem::ServerHandleFiring_Validate()
//{
//	return true;
//}
//
//
//void AItem::ServerHandleFiring_Implementation()
//{
//	const bool bShouldUpdateAmmo = CanFire();
//
//	HandleFiring();
//
//	if (bShouldUpdateAmmo)
//	{
//		// Update firing FX on remote clients
//		BurstCounter++;
//	}
//}


void AItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AItem, MyPawn);
	//TODO replication only if actually used
	DOREPLIFETIME_CONDITION(AItem, BurstCounter, COND_SkipOwner);
}


UAudioComponent* AItem::PlayItemSound(USoundCue* SoundToPlay)
{
	UAudioComponent* AC = nullptr;
	if (SoundToPlay && MyPawn)
	{
		AC = UGameplayStatics::SpawnSoundAttached(SoundToPlay, MyPawn->GetRootComponent());
	}

	return AC;
}


EItemState AItem::GetCurrentState() const
{
	return CurrentState;
}


void AItem::SetItemState(EItemState NewState)
{
	const EItemState PrevState = CurrentState;

	if (PrevState == EItemState::Active && NewState != EItemState::Active)
	{
		//TODO
		//OnBurstFinished();
	}

	CurrentState = NewState;

	if (PrevState != EItemState::Active && NewState == EItemState::Active)
	{
		//TODO
		//OnBurstStarted();
	}
}


//TODO seems to be chained to handlefiring, use timebetweenshots as cooldown counter
//void AItem::OnBurstStarted()
//{
//	// Start firing, can be delayed to satisfy TimeBetweenShots
//	const float GameTime = GetWorld()->GetTimeSeconds();
//	if (LastFireTime > 0 && TimeBetweenShots > 0.0f &&
//		LastFireTime + TimeBetweenShots > GameTime)
//	{
//		GetWorldTimerManager().SetTimer(HandleFiringTimerHandle, this, &AItem::HandleFiring, LastFireTime + TimeBetweenShots - GameTime, false);
//	}
//	else
//	{
//		HandleFiring();
//	}
//}

//TODO look above
//void AItem::OnBurstFinished()
//{
//	BurstCounter = 0;
//
//	if (GetNetMode() != NM_DedicatedServer)
//	{
//		StopSimulatingWeaponFire();
//	}
//
//	GetWorldTimerManager().ClearTimer(HandleFiringTimerHandle);
//	bRefiring = false;
//}


void AItem::DetermineItemState()
{
	EItemState NewState = EItemState::Idle;

	if (bIsEquipped)
	{
		//TODO
		/*if (bWantsToFire && CanFire())
		{
			NewState = EItemState::Active;
		}*/
	}
	else if (bPendingEquip)
	{
		NewState = EItemState::Equipping;
	}

	SetItemState(NewState);
}


float AItem::GetEquipStartedTime() const
{
	return EquipStartedTime;
}


float AItem::GetEquipDuration() const
{
	return EquipDuration;
}


float AItem::PlayItemAnimation(UAnimMontage* Animation, float InPlayRate, FName StartSectionName)
{
	float Duration = 0.0f;
	if (MyPawn)
	{
		if (Animation)
		{
			Duration = MyPawn->PlayAnimMontage(Animation, InPlayRate, StartSectionName);
		}
	}

	return Duration;
}


void AItem::StopItemAnimation(UAnimMontage* Animation)
{
	if (MyPawn)
	{
		if (Animation)
		{
			MyPawn->StopAnimMontage(Animation);
		}
	}
}


void AItem::OnEquipFinished()
{
	//AttachMeshToPawn();

	bIsEquipped = true;
	bPendingEquip = false;

	DetermineItemState();
}