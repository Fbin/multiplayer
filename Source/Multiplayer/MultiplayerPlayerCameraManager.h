// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/PlayerCameraManager.h"
#include "MultiplayerPlayerCameraManager.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYER_API AMultiplayerPlayerCameraManager : public APlayerCameraManager
{
	GENERATED_BODY()
	
	virtual void UpdateCamera(float DeltaTime) override;

public:

	AMultiplayerPlayerCameraManager(const class FObjectInitializer& ObjectInitializer);

	/* default, hip fire FOV */
	float NormalFOV;

	/* aiming down sight / zoomed FOV */
	float TargetingFOV;	
};
