// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Curves/CurveFloat.h"
#include "Components/TimelineComponent.h"
#include "Shield.generated.h"

UCLASS(ABSTRACT, Blueprintable)
class MULTIPLAYER_API AShield : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShield(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		UStaticMeshComponent* Mesh;

	float impactRadius;

	float maxImpactRadius;

	class UMaterialInstanceDynamic* dynMaterial;

	TArray<class UMaterialInstanceDynamic*> dynMaterials;

	FTimeline MyTimeline;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void HitShield(float Damage, struct FPointDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser);

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
		UStaticMeshComponent* GetShieldMesh() const;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* CurveFloat;

	UPROPERTY(EditDefaultsOnly)
		class UMaterialInstance* baseMaterial;

	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* hitParticles;

	UFUNCTION()
		void HandleProgress(float Value);
};
