// Fill out your copyright notice in the Description page of Project Settings.

#include "MultiplayerCharacterMoveComp.h"
#include "Multiplayer.h"
#include "MultiplayerCharacter.h"

float UMultiplayerCharacterMoveComp::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const AMultiplayerCharacter* CharOwner = Cast<AMultiplayerCharacter>(PawnOwner);
	if (CharOwner)
	{
		// Slow down during targeting, but don't further reduce movement speed while also crouching
		if (CharOwner->IsTargeting() && !CharOwner->GetMovementComponent()->IsCrouching())
		{
			MaxSpeed *= CharOwner->GetTargetingSpeedModifier();
		}
		else if (CharOwner->IsSprinting())
		{
			MaxSpeed *= CharOwner->GetSprintingSpeedModifier();
		}
	}

	return MaxSpeed;
}