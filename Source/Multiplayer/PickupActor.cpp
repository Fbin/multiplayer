// Fill out your copyright notice in the Description page of Project Settings.

#include "PickupActor.h"
#include "Multiplayer.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MultiplayerCharacter.h"


APickupActor::APickupActor(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//MeshComp->SetSimulatePhysics(true);
	MeshComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	MeshComp->bGenerateOverlapEvents = true;

	//bReplicateMovement = true;
	bStartActive = true;
	bAllowRespawn = true;
	RespawnDelay = 5.0f;
	RespawnDelayRange = 5.0f;

	SetReplicates(true);
}


void APickupActor::BeginPlay()
{
	Super::BeginPlay();

	RespawnPickup();
}


void APickupActor::OnUsed(APawn* InstigatorPawn)
{
	Super::OnUsed(InstigatorPawn);
	SetOwner(Cast<AActor>(InstigatorPawn));
	//UGameplayStatics::PlaySoundAtLocation(this, PickupSound, GetActorLocation());

	//OnPickedUp();

	//respawn only after destruction because of throw
	/*if (bAllowRespawn)
	{
		FTimerHandle RespawnTimerHandle;
		GetWorld()->GetTimerManager().SetTimer(RespawnTimerHandle, this, &APickupActor::RespawnPickup, RespawnDelay + FMath::RandHelper(RespawnDelayRange), false);
	}
	else
	{
		Destroy();
	}*/
	if (GetOwner()->Role < ROLE_Authority)
	{
		ServerUsed(InstigatorPawn);
		return;
	}

	OnUsedMulticast(InstigatorPawn);
}


void APickupActor::RespawnPickup()
{
	OnRespawned();
}


void APickupActor::OnRespawned()
{
	if (MeshComp)
	{
		MeshComp->SetVisibility(true);
		MeshComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	}
}


//void APickupActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
//{
//	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
//
//	//DOREPLIFETIME(APickupActor, bIsActive);
//}


void APickupActor::Drop()
{
	if (GetOwner()->Role < ROLE_Authority)
	{
		ServerDrop();
	}

	OnDropMulticast();
}


void APickupActor::Throw()
{
	if (GetOwner()->Role < ROLE_Authority)
	{
		ServerThrow();
		return;
	}

	OnDropMulticast();

	//TODO after multicast is owner itself?
	APawn* OwningPawn = Cast<APawn>(GetOwner());
	if (OwningPawn)
	{
		FVector CamLoc;
		FRotator CamRot;

		OwningPawn->Controller->GetPlayerViewPoint(CamLoc, CamRot);
		/* Re-map uint8 to 360 degrees */
		/*const float PawnViewPitch = (OwningPawn->RemoteViewPitch / 255.f)*360.f;

		FRotator NewRotation = MeshComp->GetComponentRotation();
		NewRotation.Pitch = PawnViewPitch;*/

		/* Apply physics impulse, ignores mass */
		MeshComp->AddImpulse(CamRot.Vector() * Cast<AMultiplayerCharacter>(OwningPawn)->trhowStrength, NAME_None, true);
	}
	//TODO implement explosion on collision, prevent pickup from other players
}


void APickupActor::OnUsedMulticast_Implementation(AActor* InstigatorPawn)
{
	AMultiplayerCharacter* MyPawn = Cast<AMultiplayerCharacter>(InstigatorPawn);
	MeshComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComp->SetSimulatePhysics(false);
	this->AttachToComponent(InstigatorPawn->GetRootComponent(), FAttachmentTransformRules::KeepWorldTransform);
	MyPawn->carryiedObject = this;
	MyPawn->bIsCarrying = true;
	//if (FocusActor && FocusActor->IsRootComponentMovable())
	//{
	//	/* Find the static mesh (if any) to disable physics simulation while carried
	//	Filter by objects that are physically simulated and can therefor be picked up */
	//	UStaticMeshComponent* MeshComp = Cast<UStaticMeshComponent>(FocusActor->GetComponentByClass(UStaticMeshComponent::StaticClass()));
	//	if (MeshComp && MeshComp->IsSimulatingPhysics())
	//	{
	/*		MeshComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComp->SetSimulatePhysics(false);
	}*/

	//FocusActor->AttachToComponent(this, FAttachmentTransformRules::KeepWorldTransform);
	//}
}


void APickupActor::OnDropMulticast_Implementation()
{
	MeshComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	MeshComp->SetSimulatePhysics(true);
	this->GetRootComponent()->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
}


void APickupActor::ServerDrop_Implementation()
{
	Drop();
}


bool APickupActor::ServerDrop_Validate()
{
	return true;
}


void APickupActor::ServerThrow_Implementation()
{
	Throw();
}


bool APickupActor::ServerThrow_Validate()
{
	return true;
}


void APickupActor::ServerUsed_Implementation(APawn* InstigatorPawn)
{
	OnUsed(InstigatorPawn);
}


bool APickupActor::ServerUsed_Validate(APawn* InstigatorPawn)
{
	return true;
}