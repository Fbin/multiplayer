// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types.h"
#include "Sound/SoundCue.h"
#include "Particles/ParticleSystemComponent.h"
#include "Weapon.generated.h"

UENUM()
enum class EWeaponState
{
	Idle,
	Firing,
	Equipping,
	Reloading
};

UCLASS(ABSTRACT, Blueprintable)
class MULTIPLAYER_API AWeapon : public AActor
{
	GENERATED_BODY()
	
	//new
	virtual void PostInitializeComponents() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	//new
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
		float ShotsPerMinute;
public:
	//new
	AWeapon(const FObjectInitializer& ObjectInitializer);

	void OnEquip(bool bPlayAnimation);

	virtual void OnUnEquip();

	virtual void OnEquipFinished();

	virtual void OnEnterInventory(AMultiplayerCharacter* NewOwner);

	virtual void OnLeaveInventory();

	bool IsEquipped() const;

	bool IsAttachedToPawn() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
		USkeletalMeshComponent* GetWeaponMesh() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
		class AMultiplayerCharacter* GetPawnOwner() const;

	void SetOwningPawn(AMultiplayerCharacter* NewOwner);

	float GetEquipStartedTime() const;

	float GetEquipDuration() const;

	float EquipStartedTime;

	float EquipDuration;

	bool bIsEquipped;

	bool bPendingEquip;

	FTimerHandle HandleFiringTimerHandle;

	FTimerHandle EquipFinishedTimerHandle;

	UPROPERTY(EditDefaultsOnly, Category = "Game|Weapon")
		TSubclassOf<class AWeaponPickup> WeaponPickupClass;

	/* The character socket to store this item at. */
	EInventorySlot StorageSlot;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_MyPawn)
		class AMultiplayerCharacter* MyPawn;
protected:
	/** weapon mesh: 3rd person view */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		USkeletalMeshComponent* Mesh;

	UFUNCTION()
		void OnRep_MyPawn();

	void AttachMeshToPawn(EInventorySlot Slot = EInventorySlot::Hands);

	//new UFUNCTION()
	void DetachMeshFromPawn();

public:

	FORCEINLINE EInventorySlot GetStorageSlot()
	{
		return StorageSlot;
	}

	/************************************************************************/
	/* Fire & Damage Handling                                               */
	/************************************************************************/

public:

	void StartFire();

	void StopFire();

	EWeaponState GetCurrentState() const;

protected:

	bool CanFire() const;

	FVector GetAdjustedAim() const;

	FVector GetCameraDamageStartLocation(const FVector& AimDir) const;

	FHitResult WeaponTrace(const FVector& TraceFrom, const FVector& TraceTo) const;

	/* With PURE_VIRTUAL we skip implementing the function in SWeapon.cpp and can do this in SWeaponInstant.cpp / SFlashlight.cpp instead */
	virtual void FireWeapon() PURE_VIRTUAL(AWeapon::FireWeapon, );

	UPROPERTY(EditDefaultsOnly)
		float TimeBetweenShots;

private:

	void SetWeaponState(EWeaponState NewState);

	void DetermineWeaponState();

	virtual void HandleFiring();

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerStartFire();

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerStopFire();

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerHandleFiring();

	void OnBurstStarted();

	void OnBurstFinished();

	bool bWantsToFire;

	EWeaponState CurrentState;

	bool bRefiring;

	float LastFireTime;

	/************************************************************************/
	/* Simulation & FX                                                      */
	/************************************************************************/

private:

	UFUNCTION()
		void OnRep_BurstCounter();

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundCue* FireSound;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundCue* EquipSound;

	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* MuzzleFX;

	UPROPERTY(EditDefaultsOnly)
		UAnimMontage* EquipAnim;

	UPROPERTY(EditDefaultsOnly)
		UAnimMontage* FireAnim;

	UPROPERTY(Transient)
		UParticleSystemComponent* MuzzlePSC;

	UPROPERTY(EditDefaultsOnly)
		FName MuzzleAttachPoint;

	bool bPlayingFireAnim;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_BurstCounter)
		int32 BurstCounter;

public:
	FVector GetMuzzleLocation() const;

protected:

	virtual void SimulateWeaponFire();

	virtual void StopSimulatingWeaponFire();

	FVector GetMuzzleDirection() const;

	UAudioComponent* PlayWeaponSound(USoundCue* SoundToPlay);

	float PlayWeaponAnimation(UAnimMontage* Animation, float InPlayRate = 1.f, FName StartSectionName = NAME_None);

	void StopWeaponAnimation(UAnimMontage* Animation);

	/************************************************************************/
	/* Ammo & Reloading                                                     */
	/************************************************************************/
	//TODO only reload for basic weapon, else drop
	//all new
private:

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundCue* OutOfAmmoSound;

	FTimerHandle TimerHandle_ReloadWeapon;

	FTimerHandle TimerHandle_StopReload;

protected:

	/* Time to assign on reload when no animation is found */
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		float NoAnimReloadDuration;

	/* Time to assign on equip when no animation is found */
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		float NoEquipAnimDuration;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_Reload)
		bool bPendingReload;

	void UseAmmo();

	UPROPERTY(Transient, Replicated)
		int32 CurrentAmmo;

	UPROPERTY(Transient, Replicated)
		int32 CurrentAmmoInClip;

	/* Weapon ammo on spawn */
	UPROPERTY(EditDefaultsOnly)
		int32 StartAmmo;

	UPROPERTY(EditDefaultsOnly)
		int32 MaxAmmo;

	UPROPERTY(EditDefaultsOnly)
		int32 MaxAmmoPerClip;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
		USoundCue* ReloadSound;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		UAnimMontage* ReloadAnim;

	virtual void ReloadWeapon();

	UFUNCTION(Reliable, Client)
		void ClientStartReload();

	void ClientStartReload_Implementation();

	bool CanReload();

	UFUNCTION()
		void OnRep_Reload();

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerStartReload();

	void ServerStartReload_Implementation();

	bool ServerStartReload_Validate();

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerStopReload();

	void ServerStopReload_Implementation();

	bool ServerStopReload_Validate();

public:

	virtual void StartReload(bool bFromReplication = false);

	virtual void StopSimulateReload();

	int32 GiveAmmo(int32 AddAmount);

	void SetAmmoCount(int32 NewTotalAmount);

	UFUNCTION(BlueprintCallable, Category = "Ammo")
		int32 GetCurrentAmmo() const;

	UFUNCTION(BlueprintCallable, Category = "Ammo")
		int32 GetCurrentAmmoInClip() const;

	UFUNCTION(BlueprintCallable, Category = "Ammo")
		int32 GetMaxAmmoPerClip() const;

	UFUNCTION(BlueprintCallable, Category = "Ammo")
		int32 GetMaxAmmo() const;
};