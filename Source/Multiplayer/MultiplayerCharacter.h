// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "Sound/SoundCue.h"
#include "MultiplayerCharacter.generated.h"

UCLASS()
class MULTIPLAYER_API AMultiplayerCharacter : public ACharacter
{
	GENERATED_BODY()


private:
	/* Primary camera of the player*/
	UPROPERTY(VisibleAnywhere, Category = "Camera")
		class UCameraComponent* CameraComp;

public:
	// Sets default values for this character's properties
	AMultiplayerCharacter(const class FObjectInitializer& ObjectInitializer);
	//new
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
		USoundCue* SoundTakeHit;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
		USoundCue* SoundDeath;
	//
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void PawnClientRestart() override;

	/* Stop playing all montages */
	void StopAllAnimMontages();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	//Movement
	virtual void MoveForward(float val);
	virtual void MoveRight(float val);
	void OnJumpStart();	
	void OnJumpEnd();
	void OnCrouchToggle();
	void OnSprintStart();
	void OnSprintEnd();
	//new
	virtual void OnMovementModeChanged(EMovementMode PrevMovementMode, uint8 PreviousCustomMode = 0) override;

	UPROPERTY(Transient, Replicated)
		bool bWantsToRun;

	UPROPERTY(Transient, Replicated)
		bool bIsJumping;

	UFUNCTION(BlueprintCallable, Category = "Movement")
		bool IsInitiatedJump() const;

	void SetIsJumping(bool NewJumping);

	UFUNCTION(Reliable, Server, WithValidation, Category = "Movement")
		void ServerSetIsJumping(bool NewJumping);

	//void OnLanded(const FHitResult& Hit);// override;

	/* Client/local call to update sprint state  */
	void SetSprinting(bool NewSprinting);

	/* Server side call to update actual sprint state */
	UFUNCTION(Server, Reliable, WithValidation, Category = "Movement")
		void ServerSetSprinting(bool NewSprinting);

	UFUNCTION(BlueprintCallable, Category = "Movement")
		bool IsSprinting() const;

	float GetSprintingSpeedModifier() const;

	UPROPERTY(EditDefaultsOnly, Category = "Movement")
		float SprintingSpeedModifier;


	//interaction
	UPROPERTY(Transient, Replicated)
		bool bIsCarrying;

	class APickupActor* carryiedObject;

	UPROPERTY(EditDefaultsOnly)
		int trhowStrength;

	virtual void Use();
	//new use as reference for use()
	void OnToggleCarryActor();

	UFUNCTION(Server, Reliable, WithValidation, Category = "ObjectInteraction")
		void ServerUse();

	virtual void UseItem();

	UFUNCTION(Server, Reliable, WithValidation, Category = "ObjectInteraction")
		void ServerUseItem();

	class AUsableActor* GetUsableInView();

	/*Max distance to use/focus on actors. */
	UPROPERTY(EditDefaultsOnly, Category = "ObjectInteraction")
		float MaxUseDistance;

	/* True only in first frame when focused on a new usable actor. */
	bool bHasNewFocus;

	class AUsableActor* FocusedUsableActor;


	//Playercondition
	UPROPERTY(EditDefaultsOnly, Replicated, Category = "PlayerCondition")
		float health;

	UFUNCTION(BlueprintCallable, Category = "PlayerCondition")
		void GainHealth(float AmountRestored);

	UFUNCTION(BlueprintCallable, Category = "PlayerCondition")
		float GetHealth() const;

	UFUNCTION(BlueprintCallable, Category = "PlayerCondition")
		float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "PlayerCondition")
		bool IsAlive() const;


	//take damage and death
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	virtual bool CanDie(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser) const;

	virtual bool Die(float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser);

	virtual void OnDeath(float KillingDamage, FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser);

	virtual void FellOutOfWorld(const class UDamageType& DmgType) override;

	void SetRagdollPhysics();

	virtual void PlayHit(float DamageTaken, struct FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser, bool bKilled);

	void ReplicateHit(float DamageTaken, struct FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser, bool bKilled);

	//new
	virtual void Suicide();

	virtual void KilledBy(class APawn* EventInstigator);

	/* Holds hit data to replicate hits and death to clients */
	UPROPERTY(Transient, ReplicatedUsing = OnRep_LastTakeHitInfo)
		struct FTakeHitInfo LastTakeHitInfo;

	UFUNCTION()
		void OnRep_LastTakeHitInfo();

	bool bIsDying;


	//aiming
	void OnStartTargeting();

	void OnEndTargeting();

	void SetTargeting(bool NewTargeting);

	UFUNCTION(Server, Reliable, WithValidation)
		void ServerSetTargeting(bool NewTargeting);

	UFUNCTION(BlueprintCallable, Category = "Targeting")
		bool IsTargeting() const;

	float GetTargetingSpeedModifier() const;

	UPROPERTY(Transient, Replicated)
		bool bIsTargeting;

	UPROPERTY(EditDefaultsOnly, Category = "Targeting")
		float TargetingSpeedModifier;


	//weapons and inventory
private:
	/* Attachpoint for active weapon/item in hands */
	UPROPERTY(EditDefaultsOnly, Category = "Sockets")
		FName WeaponAttachPoint;

	/* Attachpoint for items carried on the belt/pelvis. */
	UPROPERTY(EditDefaultsOnly, Category = "Sockets")
		FName PelvisAttachPoint;

	/* Attachpoint for primary weapons */
	UPROPERTY(EditDefaultsOnly, Category = "Sockets")
		FName SpineAttachPoint;

	bool bWantsToFire;

	/* Distance away from character when dropping inventory items. */
	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
		float DropItemDistance;

	/* Mapped to input */
	void OnStartFire();

	/* Mapped to input */
	void OnStopFire();

	//new
	void OnReload();

	void StartWeaponFire();

	void StopWeaponFire();

	void DestroyInventory();

	/* Mapped to input. Drops current weapon */
	void DropWeapon();

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerDropWeapon();

	void DropItem();

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerDropItem();

	//new
	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
		float DropWeaponMaxDistance;

public:
	bool WeaponSlotAvailable(EInventorySlot CheckSlot);

	bool ItemSlotAvailable(EInventorySlot CheckSlot);

	FName GetInventoryAttachPoint(EInventorySlot Slot) const;

	UPROPERTY(Transient, Replicated)
		TArray<AActor*> Inventory;

	void SpawnDefaultInventory();


	bool CanFire() const;

	//new
	bool CanReload() const;

	UFUNCTION(BlueprintCallable, Category = "Weapon")
		bool IsFiring() const;

	void SetCurrentWeapon(class AWeapon* newWeapon, bool bPlayAnimation, class AWeapon* LastWeapon = nullptr);

	void SetCurrentItem(class AItem* newItem, bool bPlayAnimation, class AItem* lastItem = nullptr);

	void SetCurrentAmmo(class AAmmo* newAmmo, class AAmmo* lastAmmo = nullptr);

	void EquipWeapon(AWeapon* Weapon);

	void EquipItem(AItem* Item);

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerEquipWeapon(AWeapon* Weapon);

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerEquipItem(AItem* Item);

	/* OnRep functions can use a parameter to hold the previous value of the variable. Very useful when you need to handle UnEquip etc. */
	UFUNCTION()
		void OnRep_CurrentWeapon(AWeapon* LastWeapon);

	UFUNCTION()
		void OnRep_CurrentItem(AItem* LastItem);

	UFUNCTION()
		void OnRep_CurrentAmmo(AAmmo* LastAmmo);

	void AddWeapon(class AWeapon* Weapon);

	void AddItem(class AItem* Item);

	void RemoveWeapon(class AWeapon* Weapon);

	void RemoveItem(class AItem* Item);

	UPROPERTY(Transient, ReplicatedUsing = OnRep_CurrentWeapon)
		class AWeapon* CurrentWeapon;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_CurrentItem)
		class AItem* CurrentItem;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_CurrentAmmo)
		class AAmmo* CurrentAmmo;

	/* The default weapons to spawn with */
	UPROPERTY(EditDefaultsOnly, Category = Inventory)
		TArray<TSubclassOf<class AActor>> DefaultInventoryClasses;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class AAmmo> ammo;
};
