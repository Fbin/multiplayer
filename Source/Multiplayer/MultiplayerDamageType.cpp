// Fill out your copyright notice in the Description page of Project Settings.

#include "MultiplayerDamageType.h"

UMultiplayerDamageType::UMultiplayerDamageType(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	/* We apply this modifier based on the physics material setup to the head of the enemy PhysAsset */
	HeadDmgModifier = 2.0f;
	LimbDmgModifier = 0.5f;

	bCanDieFrom = true;
}


bool UMultiplayerDamageType::GetCanDieFrom()
{
	return bCanDieFrom;
}


float UMultiplayerDamageType::GetHeadDamageModifier()
{
	return HeadDmgModifier;
}

float UMultiplayerDamageType::GetLimbDamageModifier()
{
	return LimbDmgModifier;
}