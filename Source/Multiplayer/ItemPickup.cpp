// Fill out your copyright notice in the Description page of Project Settings.

#include "ItemPickup.h"
#include "MultiplayerCharacter.h"

AItemPickup::AItemPickup(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	OnActorBeginOverlap.AddDynamic(this, &AItemPickup::OnPickup);
	//TODO asign default values
}


void AItemPickup::OnUsed(APawn* InstigatorPawn)
{
	// Plays pickup sound from base class
	Super::OnUsed(InstigatorPawn);

	AMultiplayerCharacter* Pawn = Cast<AMultiplayerCharacter>(InstigatorPawn);
	//TODO maybe consume instantly
	/*if (Pawn)
	{
		Pawn->ConsumeFood(Nutrition);
	}*/

	// Remove from level
	Destroy();
}

void AItemPickup::OnPickup(AActor* OverlappedActor, AActor* InstigatorPawn)
{
	AMultiplayerCharacter* MyPawn = Cast<AMultiplayerCharacter>(InstigatorPawn);
	if (MyPawn)
	{
		/* Fetch the default variables of the class we are about to pick up and check if the storage slot is available on the pawn. */
		if (MyPawn->ItemSlotAvailable(ItemClass->GetDefaultObject<AItem>()->GetStorageSlot()))
		{
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.bNoFail = true;
			AItem* NewItem = GetWorld()->SpawnActor<AItem>(ItemClass, SpawnInfo);

			MyPawn->AddItem(NewItem);

			Destroy();
		}
		{
			// TODO: Add warning to user HUD.
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, "Item slot taken!");
		}
	}
}