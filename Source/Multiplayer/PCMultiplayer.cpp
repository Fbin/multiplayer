// Fill out your copyright notice in the Description page of Project Settings.

#include "PCMultiplayer.h"
#include "Multiplayer.h"
#include "MultiplayerGI.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "MultiplayerPlayerCameraManager.h"

void APCMultiplayer::BeginPlay()
{
	Super::BeginPlay();

	//Is handled by levelblueprint
	//PlayerSettingsSave = "PlayerSettingsSave";

	//if (wMainMenu)
	//{
	//	//MyMainMenu = CreateWidget<UUserWidget>(this, wMainMenu);
	//	if (UGameplayStatics::DoesSaveGameExist(PlayerSettingsSave, 0))
	//	{
	//		if (MyMainMenu)
	//		{
	//			//MyMainMenu->AddToViewport();
	//			Cast<UMultiplayerGI>(GetGameInstance())->ShowMainMenu();
	//		}

	//	}
	//	else
	//	{
	//		Cast<UMultiplayerGI>(GetGameInstance())->ShowPlayerSettings();
	//	}

	//	bShowMouseCursor = true;
	//}

	PlayerCameraManagerClass = AMultiplayerPlayerCameraManager::StaticClass();
}