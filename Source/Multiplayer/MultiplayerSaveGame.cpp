// Fill out your copyright notice in the Description page of Project Settings.

#include "MultiplayerSaveGame.h"
#include "Multiplayer.h"

void UMultiplayerSaveGame::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to every client, no special condition required
	DOREPLIFETIME(UMultiplayerSaveGame, S_PlayerInfo);
}