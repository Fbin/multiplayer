// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "MultiplayerDamageType.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYER_API UMultiplayerDamageType : public UDamageType
{
	GENERATED_BODY()
	
	UMultiplayerDamageType(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditDefaultsOnly)
		bool bCanDieFrom;

	UPROPERTY(EditDefaultsOnly)
		float HeadDmgModifier;

	UPROPERTY(EditDefaultsOnly)
		float LimbDmgModifier;

public:

	bool GetCanDieFrom();

	float GetHeadDamageModifier();

	float GetLimbDamageModifier();
};
