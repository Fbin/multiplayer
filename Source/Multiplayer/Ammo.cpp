// Fill out your copyright notice in the Description page of Project Settings.

#include "Ammo.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Multiplayer.h"
#include "Kismet/GameplayStatics.h"
#include "MultiplayerCharacter.h"
#include "ImpactEffect.h"
#include "PCMultiplayer.h"
#include "Weapon.h"
#include "WeaponActor.h"
#include "Shield.h"
#include "FluidShield.h"
#include "Particles/ParticleSystemComponent.h"
#include "DrawDebugHelpers.h"

// Sets default values
AAmmo::AAmmo(const class FObjectInitializer& PCIP)
	: Super(PCIP)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	Mesh = PCIP.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("BulletMesh"));
	Mesh->SetCollisionObjectType(COLLISION_WEAPON);
	Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Mesh->SetCollisionResponseToAllChannels(ECR_Overlap);
	Mesh->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);//TODO check if actually right for shield
	Mesh->SetWorldRotation(FRotator(-90.f, 0.f, 0.f));
	Mesh->SetSimulatePhysics(true);
	//Mesh->SetMassOverrideInKg(NAME_None, 0.02, true);
	RootComponent = Mesh;

	Emitter = PCIP.CreateDefaultSubobject<UParticleSystemComponent>(this, TEXT("ShotParticles"));
	Emitter->SetWorldRotation(FRotator(-90.f, 0.f, 0.f));
	Emitter->SetupAttachment(Mesh);

	BulletHeadPoint = PCIP.CreateDefaultSubobject<UArrowComponent>(this, TEXT("BulletHeadPoint"));
	BulletHeadPoint->SetWorldRotation(FRotator(-90.f, 0.f, 0.f));
	BulletHeadPoint->SetupAttachment(Mesh);
	
	HitDamageModifier = 1.0f;
	WeaponRangeModifier = 1.0f;
	WeaponAccuracyModifier = 1.0f;
	WeaponShotAmountModifier = 1.0f;
	WeaponShotScatterModifier = 1.0f;
	AllowedViewDotHitDir = 0.8f;
	ClientSideHitLeeway = 200.0f;

	TotalDamage = 0.f;
}


void AAmmo::BeginPlay()
{
	Super::BeginPlay();

	/*if (Role == ROLE_Authority)
	{
		SetValues();
	}
	else
	{
		ServerSetValues();
	}*/
}


void AAmmo::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (TotalDamage == 0)
	{
		if (Role == ROLE_Authority)
		{
			SetValues();
		}
		else
		{
			ServerSetValues();
		}
		FVector norm = GetActorUpVector();
		norm.Normalize();
		Mesh->AddImpulse(norm*TotalRange);
	}
	FVector TraceStart = GetBulletHeadLocation();
	FVector TraceEnd = TraceStart + GetActorUpVector() * 10;
	const FHitResult Impact = HitTrace(TraceStart, TraceEnd);
	//DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, false, 100.0f);
	if (Impact.bBlockingHit)
	{
		ProcessInstantHit(Impact, GetBulletHeadLocation(), GetActorForwardVector());
	}
}


FHitResult AAmmo::HitTrace(const FVector& TraceFrom, const FVector& TraceTo) const
{
	FCollisionQueryParams TraceParams(TEXT("WeaponTrace"), true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, TraceFrom, TraceTo, COLLISION_WEAPON, TraceParams);

	return Hit;
}


FVector AAmmo::GetBulletHeadLocation() const
{
	return BulletHeadPoint->GetComponentLocation();
}


void AAmmo::ProcessInstantHit(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir)
{
	bool a = MyPawn->IsLocallyControlled();
	ENetMode b = GetNetMode();
	if (MyPawn && MyPawn->IsLocallyControlled() && GetNetMode() == NM_Client)
	{
		if (Impact.GetActor() && Impact.GetActor()->GetRemoteRole() == ROLE_Authority)
		{
			ServerNotifyHit(Impact, ShootDir);
		}
		else if (Impact.GetActor() == nullptr)
		{
			if (Impact.bBlockingHit)
			{
				ServerNotifyHit(Impact, ShootDir);
			}
			else
			{
				//ServerNotifyMiss(ShootDir);
			}
		}
	}

	ProcessInstantHitConfirmed(Impact, Origin, ShootDir);
}


bool AAmmo::ServerNotifyHit_Validate(const FHitResult& Impact, FVector_NetQuantizeNormal ShootDir)
{
	return true;
}


void AAmmo::ServerNotifyHit_Implementation(const FHitResult& Impact, FVector_NetQuantizeNormal ShootDir)
{
	// If we have an instigator, calculate the dot between the view and the shot
	if (Instigator && (Impact.GetActor() || Impact.bBlockingHit))
	{
		const FVector Origin = GetBulletHeadLocation();
		const FVector ViewDir = (Impact.Location - Origin).GetSafeNormal();

		const float ViewDotHitDir = FVector::DotProduct(Instigator->GetViewRotation().Vector(), ViewDir);
		if (ViewDotHitDir > AllowedViewDotHitDir)
		{
			// TODO: Check for weapon state

			if (Impact.GetActor() == nullptr)
			{
				if (Impact.bBlockingHit)
				{
					ProcessInstantHitConfirmed(Impact, Origin, ShootDir);
				}
			}
			else if (Impact.GetActor()->IsRootComponentStatic() || Impact.GetActor()->IsRootComponentStationary())
			{
				ProcessInstantHitConfirmed(Impact, Origin, ShootDir);
			}
			else
			{
				const FBox HitBox = Impact.GetActor()->GetComponentsBoundingBox();

				FVector BoxExtent = 0.5 * (HitBox.Max - HitBox.Min);
				BoxExtent *= ClientSideHitLeeway;

				BoxExtent.X = FMath::Max(20.0f, BoxExtent.X);
				BoxExtent.Y = FMath::Max(20.0f, BoxExtent.Y);
				BoxExtent.Z = FMath::Max(20.0f, BoxExtent.Z);

				const FVector BoxCenter = (HitBox.Min + HitBox.Max) * 0.5;

				// If we are within client tolerance
				if (FMath::Abs(Impact.Location.Z - BoxCenter.Z) < BoxExtent.Z &&
					FMath::Abs(Impact.Location.X - BoxCenter.X) < BoxExtent.X &&
					FMath::Abs(Impact.Location.Y - BoxCenter.Y) < BoxExtent.Y)
				{
					ProcessInstantHitConfirmed(Impact, Origin, ShootDir);
				}
			}
		}
	}

	// TODO: UE_LOG on failures & rejection
}


void AAmmo::ProcessInstantHitConfirmed(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir)
{
	if (ShouldDealDamage(Impact.GetActor()))
	{
		DealDamage(Impact, ShootDir);
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::SanitizeFloat(Cast<AMultiplayerCharacter>(Impact.GetActor())->GetHealth()));
	}
	else if (Cast<AShield>(Impact.GetActor()))
	{
		FPointDamageEvent PointDmg;
		PointDmg.DamageTypeClass = DamageType;
		PointDmg.HitInfo = Impact;
		PointDmg.ShotDirection = ShootDir;
		PointDmg.Damage = TotalDamage;

		Cast<AShield>(Impact.GetActor())->HitShield(PointDmg.Damage, PointDmg, MyPawn->Controller, this);
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, "Shield Hit");
	}
	else if (Cast<AFluidShield>(Impact.GetActor()))
	{
		FPointDamageEvent PointDmg;
		PointDmg.DamageTypeClass = DamageType;
		PointDmg.HitInfo = Impact;
		PointDmg.ShotDirection = ShootDir;
		PointDmg.Damage = TotalDamage;

		//Cast<AFluidShield>(Impact.GetActor())->TakeDamage(PointDmg.Damage, PointDmg, MyPawn->Controller, this);
		Cast<AFluidShield>(Impact.GetActor())->HitShield(PointDmg.Damage, PointDmg, MyPawn->Controller, this);
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, "Shield Hit");
	}

	//// Play FX on remote clients
	//if (Role == ROLE_Authority)
	//{
	//	HitOriginNotify = Origin;
	//}

	//// Play FX locally
	//if (GetNetMode() != NM_DedicatedServer)
	//{
	//	SimulateInstantHit(Origin);
	//}

	Destroy();
}


bool AAmmo::ShouldDealDamage(AActor* TestActor) const
{
	// If we are an actor on the server, or the local client has authoritative control over actor, we should register damage.
	if (TestActor && Cast<AMultiplayerCharacter>(TestActor))
	{
		if (GetNetMode() != NM_Client ||
			TestActor->Role == ROLE_Authority ||
			TestActor->bTearOff)
		{
			return true;
		}
	}

	return false;
}


void AAmmo::DealDamage(const FHitResult& Impact, const FVector& ShootDir)
{
	FPointDamageEvent PointDmg;
	PointDmg.DamageTypeClass = DamageType;
	PointDmg.HitInfo = Impact;
	PointDmg.ShotDirection = ShootDir;
	PointDmg.Damage = TotalDamage;

	Impact.GetActor()->TakeDamage(PointDmg.Damage, PointDmg, MyPawn->Controller, this);
}


void AAmmo::SimulateInstantHit(const FVector& Origin)
{
	const FVector StartTrace = Origin;
	const FVector AimDir = GetActorForwardVector();
	const FVector EndTrace = StartTrace + (AimDir * 10);

	const FHitResult Impact = HitTrace(StartTrace, EndTrace);
	if (Impact.bBlockingHit)
	{
		SpawnImpactEffects(Impact);
		//SpawnTrailEffects(Impact.ImpactPoint);
	}
	else
	{
		//SpawnTrailEffects(EndTrace);
	}
}


void AAmmo::SpawnImpactEffects(const FHitResult& Impact)
{
	if (ImpactTemplate && Impact.bBlockingHit)
	{
		// TODO: Possible re-trace to get hit component that is lost during replication.

		/* This function prepares an actor to spawn, but requires another call to finish the actual spawn progress. This allows manipulation of properties before entering into the level */
		//TODO check if owner or instigator have to be set
		AImpactEffect* EffectActor = GetWorld()->SpawnActorDeferred<AImpactEffect>(ImpactTemplate, FTransform(Impact.ImpactPoint.Rotation(), Impact.ImpactPoint, FVector::OneVector));
		if (EffectActor)
		{
			EffectActor->SurfaceHit = Impact;
			//TODO doesnt seem to stop particles from spawning
			UGameplayStatics::FinishSpawningActor(EffectActor, FTransform(Impact.ImpactNormal.Rotation(), Impact.ImpactPoint));
		}
	}
}


void AAmmo::SetOwningPawn(AMultiplayerCharacter* NewOwner)
{
	if (MyPawn != NewOwner)
	{
		Instigator = NewOwner;
		MyPawn = NewOwner;
		// Net owner for RPC calls.
		SetOwner(NewOwner);
	}
}


void AAmmo::OnRep_MyPawn()
{
	/*if (MyPawn)
	{
		OnEnterInventory(MyPawn);
	}
	else
	{
		OnLeaveInventory();
	}*/
}


void AAmmo::SetValues()
{
	TotalDamage = Cast<AWeaponActor>(MyPawn->CurrentWeapon)->HitDamage * HitDamageModifier;
	TotalRange = Cast<AWeaponActor>(MyPawn->CurrentWeapon)->WeaponRange * WeaponRangeModifier;
	TotalAccuracy = Cast<AWeaponActor>(MyPawn->CurrentWeapon)->WeaponZoom - WeaponAccuracyModifier;
}


void AAmmo::SetMyPawn(AMultiplayerCharacter* NewPawn)
{
	MyPawn = NewPawn;
}


float AAmmo::GetScatterAmount()
{
	return WeaponShotScatterModifier;
}


float AAmmo::GetShotAmount()
{
	return WeaponShotAmountModifier;
}


void AAmmo::ServerSetValues_Implementation()
{
	SetValues();
}


bool AAmmo::ServerSetValues_Validate()
{
	return true;
}


void AAmmo::OnRep_HitLocation()
{
	// Played on all remote clients
	SimulateInstantHit(HitOriginNotify);
}


void AAmmo::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AAmmo, HitOriginNotify, COND_SkipOwner);
}